#include <gpio.h>
#include "inc/hw_regaccess.h"
#include "bootloader.h"
#include <msp430f5510.h>

#define UUID_ADR  0x8600

#define LED_RGB_PORT GPIO_PORT_P6
#define LED_R_PIN GPIO_PIN0
#define LED_G_PIN GPIO_PIN1
#define LED_B_PIN GPIO_PIN2

#define IAP_YES                 1
#define IAP_NO                  0
#define PACK_DATA_LEN           512

#define OPEN_W()  P6OUT &= (~0x07)//open
#define CLOSE_W()  P6OUT |=0x07//close

#define IS_LIGHT_ON()     (P1IN & GPIO_PIN2)

#define EEPROM_ADDRESS  0x50
#define EEPROM_ID_ADDRESS   0x58
#define PAGE_BYTES 128
#define PAGE_ADR_OFFSET 7
#define V_EEPROM_PROT  GPIO_PORT_P2
#define V_EEPROM_PIN   GPIO_PIN0
#define EEPROM_POWER_ON()       P2OUT |= 0x01
#define EEPROM_POWER_OFF()      P2OUT &= ~0x01

#define EEPROM_DATA_ADDR        128

// state machine 
#define STATE_NONE 0
#define STATE_RECVD_HEADER 1
#define STATE_RECVD_INDEX 2
#define STATE_RECVD_ADDR 3
#define STATE_RECVD_LEN 4
#define STATE_RECVD_DATA 5
#define STATE_RECVD_CRC 6

int8_t BootLoaderState = STATE_NONE;
uint8_t CmdNum = 0;
uint8_t CmdIdx = 0;
uint8_t UpdateFlag = 0;

uint8_t indexNum;
uint8_t data[PACK_DATA_LEN];
//uint8_t *data = (uint8_t *)0x1C00;
uint8_t tmp[PACK_DATA_LEN];
uint16_t lenth;
uint16_t addr;
uint16_t CRC_Value;
uint8_t out[32];
uint8_t go_count;

volatile uint16_t timeA2_couter;
volatile uint16_t timeA2_delay;

uint8_t bit_index = 0;
uint16_t bytes_recved = 0;
uint8_t byte_data = 0;
uint16_t data_len = 0;

void delay(uint16_t ms)
{
//  timeA2_delay = (10*ms);
//  TA2CCR0 = 800-1;      //100uS  
//  TA2CCTL0 = CCIE;//open
//  LPM3;
//  TA2CCTL0 &= ~CCIE;//stop
//  timeA2_delay = 0;
  while(ms){
  __delay_cycles(8000);//1000us
  ms--;
  }
  
}

#pragma vector=TIMER2_A0_VECTOR
__interrupt void TIMER2_A0_ISR(void)
{    
    timeA2_couter++;
//    if(timeA2_delay){
//      timeA2_delay--;
//      if(0==timeA2_delay){
//        TA2CCR0 = 80-1;      //10uS  
//        LPM3_EXIT;
//      }
//    }
}


void wrt_flash_byte(uint16_t addr,uint8_t d)
{

  char *Flash_ptr = (char *)addr;        // Initialize Flash pointer

  FCTL3 = FWKEY;                            // Clear Lock bit
  FCTL1 = FWKEY+WRT;                        // Set WRT bit for write operation

  *Flash_ptr++ = d;                   // Write a word to flash

  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;                       // Set LOCK bit
  
}

void wrt_flash_arry(uint16_t addr, uint8_t* d, uint16_t len)
{
  uint16_t i;
  char *Flash_ptr = (char *)addr;        // Initialize Flash pointer

  FCTL3 = FWKEY;                            // Clear Lock bit
  FCTL1 = FWKEY+WRT;                        // Set WRT bit for write operation
  for(i=0;i<len;i++){
	  *Flash_ptr++ = d[i];           // Write a word to flash
  }

  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;                       // Set LOCK bit

}



#define POLY 0x8408
uint16_t crc16(uint8_t *data_p, uint16_t l)
{
      uint8_t i;
      uint16_t d;
      uint16_t crc = 0xffff;

      if (l == 0)
            return (~crc);

      do
      {
            for (i=0, d=0xff & *data_p++;
                 i < 8;
                 i++, d >>= 1)
            {
                  if ((crc & 0x0001) ^ (d & 0x0001))
                        crc = (crc >> 1) ^ POLY;
                  else  crc >>= 1;
            }
      } while (--l);

      crc = ~crc;
      d = crc;
      crc = (crc << 8) | (d >> 8 & 0xff);

      return (crc);
}


int8_t crc_cmd(void)
{
  uint16_t i;
  uint16_t crc = 0;
  
    
  // START
  if (indexNum == 0)
  {
    tmp[0] = 0x00;
    tmp[1] = 0x00;
    tmp[2] = 0x00;
    tmp[3] = 0x00;
    tmp[4] = 0x01;      // len
    tmp[5] = data[0];
    
    crc = crc16(tmp, 6);
    
    if (crc == CRC_Value)
    {
      return 1;
    }
    return 0;
  }
  
  //DATA
  tmp[0] = indexNum;
  tmp[1] = ((uint8_t *)&addr)[0];
  tmp[2] = ((uint8_t *)&addr)[1];
  tmp[3] = ((uint8_t *)&lenth)[0];
  tmp[4] = ((uint8_t *)&lenth)[1];
  for (i = 0; i < lenth; i++)
  {
    (tmp + 5)[i] = data[i];
  }
  crc = crc16(tmp, 5 + i);

  if (crc == CRC_Value)
  {
    return 1;
  }
  return 0;
}

void flash_erase_seg(uint16_t segAddr)
{
  char *Flash_ptr = (char *)segAddr;        // Initialize Flash pointer
  FCTL3 = FWKEY;                            // Clear Lock bit
  FCTL1 = FWKEY+ERASE;                      // Set Bank Erase bit
  *Flash_ptr = 0;// Dummy erase byte
  FCTL3 = FWKEY+LOCK;                       // Set LOCK bit
}

void sendArry(uint8_t* data,uint8_t len)
{
  uint8_t tempValue,index,num=0;
  index=0;
  tempValue=data[0];
    while(index<len){
      if((index==0)&&(num == 0)){
          OPEN_W();
          //delay(3000);//30ms
          delay(32);
          CLOSE_W();
          //delay(500);
          delay(5);
      }
       //data
      OPEN_W();
      if((tempValue>>num)&0x01)
      {
         //delay(400);//1--->4ms
        delay(4);
      }
      else
      {
         //delay(1500);//0--->15ms
        delay(15);
      }
      CLOSE_W();
      num++;
      if(num==8) {
        index++;
        num = 0;
        tempValue=data[index];
      }
      //delay(1000);//low -->10ms
      delay(10);
  }
}

static void select_xt1(void)
{
    // Set up XT1
    P5SEL |= BIT4+BIT5;                       // Select XT1
    UCSCTL6 &= ~(XT1OFF);                     // XT1 On
    UCSCTL6 |= XCAP_3;                        // Internal load cap 
    UCSCTL6 |= XT1DRIVE_3;
    //UCSCTL6 &= ~(0x0020u);                    //LOW fRE MODE
    do
    {
        UCSCTL7 &= ~XT1LFOFFG;                  // 清楚XT1错误标记
    }while (UCSCTL7&XT1LFOFFG);                 // 检测XT1错误标记

}

static void dco_config(void)
{
    __bis_SR_register(SCG0);    // Disable the FLL control loop	禁止FLL功能
    UCSCTL0 = 0x0000;           // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;        // Select DCO range 16MHz operation
    UCSCTL2 = FLLD_1 + 243;     // Set DCO Multiplier for 8MHz
                                // (N + 1) * FLLRef = Fdco
                                // (243 + 1) * 32768 = 8MHz         计算公式
                                // Set FLL Div = fDCOCLK/2
    __bic_SR_register(SCG0);    // Enable the FLL control loop	使能FLL功能

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 8 MHz / 32,768 Hz = 250000 = MCLK cycles for DCO to settle
    __delay_cycles(250000);

    // Loop until XT1,XT2 & DCO fault flag is cleared
    do
    {
       // UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
                               // Clear XT2,XT1,DCO fault flags	清除所有错误标志位
        SFRIFG1 &= ~OFIFG;     // Clear fault flags		清除振荡器错误
    }while (SFRIFG1&OFIFG);   // Test oscillator fault flag    等待
}

void clock_config(void)
{
    WDTCTL = WDTPW + WDTHOLD;                   // 停止看门狗
    
    //关闭usb,达到最低功耗。
    USBKEYPID = USBKEY;
    USBPWRCTL = 0;
    USBKEYPID = 0;

    __delay_cycles(250000);
    //UCS_LFXT1Start(,UCS_XT1_DRIVE3,UCS_XCAP_3);
    select_xt1();              // 选择XT1
    dco_config();             // ACLK = XT1 = 32.768K MCLK = SMCLK = 8000K
    
}


void init_sys()
{
    clock_config();
    P6SEL &= ~(LED_R_PIN+LED_G_PIN+LED_B_PIN);                        // P3.2@UCB1SCL
    P6DIR |= (LED_R_PIN+LED_G_PIN+LED_B_PIN);
    
    P2SEL &= ~(V_EEPROM_PIN);
    P2DIR |= (V_EEPROM_PIN);
    
    // init TIM
    
//    //TA1CCR0 = FLASH_LIGHT_COMPARE_VALUE-1;      //1MS    
//    TA1CTL |= TASSEL__SMCLK;
//    TA1CTL |= MC__CONTINUOUS;
//    TA1CTL |= ID__8;
    
    //TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
    TA2CCR0 = 80-1;      //10uS                   
    TA2CTL = TASSEL_2 + MC_1 + TACLR ;        // smCLK, upmode, clear TAR
    

    CLOSE_W();
}

int8_t recv_bit(uint8_t s)
{
  go_count = 0;
  while(IS_LIGHT_ON());
  TA2CCTL0 &= ~CCIE;
  
  if((timeA2_couter<=26) && (timeA2_couter > 16))
  {
    return 0;
  }
  
  return 1;
}


static int8_t recv_cmd_blk(void)
{
  uint8_t s = 0;
  
  while (1)
  {
    // wait the light
    while( ! IS_LIGHT_ON() );
        
    timeA2_couter=0;
    TA2CCTL0 = CCIE;//open
    if (BootLoaderState == STATE_NONE)
    {   // want START signal
      
      while(IS_LIGHT_ON());
      TA2CCTL0 &= ~CCIE;
      
      if((timeA2_couter >= 90)&&(timeA2_couter <= 130))
      {
        // Get start signal
        /*
        for ( i = 0; i < PACK_DATA_LEN; i++)
        {
          data[i] = 0;
        }
        */
        addr = 0;
        CRC_Value = 0;
        indexNum = 0;
        lenth = 0;
        
        bit_index = 0;
        bytes_recved = 0;
        byte_data = 0;
        data_len = 0;
        go_count = 0;
        
        BootLoaderState = STATE_RECVD_HEADER;
      }
      else if ((timeA2_couter <= 47) && (timeA2_couter >= 41))
      {
        go_count++;
        if (go_count >= 3)
        {
          out[0] = 0xFF;
          sendArry(out, 1);
          go_count = 0;
        }
        BootLoaderState = STATE_NONE;
      }
      else 
      {
        BootLoaderState = STATE_NONE;
      }
    } 
    else if (BootLoaderState == STATE_RECVD_HEADER)
    {   // want a byte for index
      
      if (recv_bit(s))
      {
        byte_data |= (1 << bit_index);
      }
      bit_index++;
      
      if(bit_index == 8)
      {
        bit_index = 0;
        bytes_recved++;
      }
      if (bytes_recved == 1)
      {
        // 1 byte for index
        indexNum = byte_data;
        BootLoaderState = STATE_RECVD_INDEX;
        bytes_recved = 0;
        bit_index = 0;
        byte_data = 0;
      }
    }
    else if (BootLoaderState == STATE_RECVD_INDEX)
    {   // want 2 bytes for addr
      
      if (recv_bit(s))
      {
        byte_data |= (1 << bit_index);
      }
      bit_index++;
      if(bit_index == 8)
      {
        bit_index = 0;
        bytes_recved++;
        if (bytes_recved == 1)
        {
          addr = (byte_data << 8);
          byte_data = 0;
        }
        else if (bytes_recved == 2)
        {
          // recv 2 bytes for addr
          addr |= byte_data;
          
          BootLoaderState = STATE_RECVD_ADDR;
          bytes_recved = 0;
          byte_data = 0;
          bit_index = 0;
        }
      }
      
    }
    else if (BootLoaderState == STATE_RECVD_ADDR)
    {   // want 2 bytes for len
      
      if (recv_bit(s))
      {
        byte_data |= (1 << bit_index);
      }
      bit_index++;
      if(bit_index == 8)
      {
        bit_index = 0;
        bytes_recved++;
          
        if (bytes_recved == 1)
        {
          lenth = (byte_data << 8);
          byte_data = 0;
        }
        else if (bytes_recved == 2)
        {
          lenth |= byte_data;
          BootLoaderState = STATE_RECVD_LEN;
          bytes_recved = 0;
          byte_data = 0;
          data_len = 0;
          bit_index = 0;
          
          if (lenth > PACK_DATA_LEN)
          {
            return -1;
          }
        }
      }
      
    }
    else if (BootLoaderState == STATE_RECVD_LEN)
    {   // want n bytes for data
      
      if (recv_bit(s))
      {
        byte_data |= (1 << bit_index);
      }
      bit_index++;
      if(bit_index == 8)
      {
        bit_index = 0;
        bytes_recved++;
        data_len++;
        if (bytes_recved)
        {
          data[data_len - 1] = byte_data;
          byte_data = 0;
          bytes_recved = 0;
        }
        if (data_len == lenth)
        {
          // get all data
          BootLoaderState = STATE_RECVD_DATA;
          data_len = 0;
          bytes_recved = 0;
          byte_data = 0;
          bit_index = 0;
          
        }
      }
      
    }
    else if (BootLoaderState == STATE_RECVD_DATA)
    {   // want 2 bytes for crc16
      
      if (recv_bit(s))
      {
        byte_data |= (1 << bit_index);
      }
      bit_index++;
      if(bit_index == 8)
      {
        bit_index = 0;
        bytes_recved++;
        if (bytes_recved == 1)
        {
          CRC_Value = (byte_data << 8);
          byte_data = 0;
        }
        else if (bytes_recved == 2)
        {
          CRC_Value |= byte_data;
          BootLoaderState = STATE_RECVD_CRC;
          bytes_recved = 0;
          byte_data = 0;
          bit_index = 0;
        }
      }
      
    }
    
    if (BootLoaderState == STATE_RECVD_CRC)
    {
      // we get a complete command block
      return 0;
    }
  }
}

void (*app_exec)(void) = (void (*)(void)) BL_APP_ADDR;

int main()
{
  uint32_t flash_addr = 0;
  uint16_t crc = 0;
  uint16_t *hp;
  
  timeA2_couter = 0;
  __disable_interrupt();
  init_sys();
  __delay_cycles(250000);
  
  OPEN_W();
  __delay_cycles(250000);
  CLOSE_W();
  
  UpdateFlag = (*((uint8_t *)BL_CONF_ADDR));
  if (UpdateFlag == 0x00)
  {
    app_exec();
    // NOT reached
    while(1);
  }
  __enable_interrupt(); // Timer
  // in BootLoader
  //out[0] = 0xFF;
  //sendArry(out, 1);
  
  
  while (1)
  {
    if (0 == recv_cmd_blk())
    {
      if ( ! crc_cmd())
      {
        // this command not right
        BootLoaderState = STATE_NONE;
        continue;
      }
      
      // crc OK !
      
      CmdIdx = indexNum;
      
      if (CmdIdx == 0)  // START
      {
        CmdNum = data[0];
        for (flash_addr = BL_APP_ADDR; flash_addr <= 0xFE00; flash_addr += 512)
        {
          flash_erase_seg(flash_addr + 1);
        } 
        hp = (uint16_t *)TIMER2_A0_ISR;
        // set reset handler
        wrt_flash_byte(0xFFFE, 0x00);
        wrt_flash_byte(0xFFFF, 0x10);
        wrt_flash_byte(0xFFD8, (uint8_t)((uint16_t)hp & 0xFF));
        wrt_flash_byte(0xFFD9, (uint8_t)(((uint16_t)hp >> 8) & 0xFF));
        

        out[0] = indexNum;
        out[1] = (*((uint8_t *)0x8000)) & 0xFF;
        out[2] = (*((uint8_t *)0x8001)) & 0xFF;
        out[3] = (*((uint8_t *)0x8002)) & 0xFF;
        out[4] = (*((uint8_t *)0x8003)) & 0xFF;
        
        crc = crc16(out, 5);
        
        out[5] = crc & 0xFF;
        out[6] = (crc >> 8) & 0xFF;
        sendArry(out, 7);
        
      }
      else      // DATA
      {
        if (addr < BL_APP_ADDR)
        {
          // erase the DEV ID
          flash_erase_seg(BL_DEVID_ADDR + 1);
          
        }
        
        wrt_flash_arry(addr, data, lenth);
        
        out[0] = indexNum;
        
        crc = crc16(out, 1);
        
        out[1] = crc & 0xFF;
        out[2] = (crc >> 8) & 0xFF;
        sendArry(out, 3);
        
      }
      if (CmdNum == CmdIdx)
      {
        // All DATA write to flash
        flash_erase_seg(BL_CONF_ADDR + 1);
        wrt_flash_byte( BL_CONF_ADDR, 0x00);
        app_exec();
        // NOT reached
        while(1);
      }
      BootLoaderState = STATE_NONE;
    }
    else
    {
      // recv command error
      BootLoaderState = STATE_NONE;
    }
  }
  
}


