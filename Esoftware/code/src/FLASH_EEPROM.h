/*
 * FLASH_EEPROM.h
 *
 *  Created on: 2013-9-12
 *      Author: SonicZY
 */

#ifndef FLASH_EEPROM_H_
#define FLASH_EEPROM_H_

/*
 * 32k flash == 64seg * 512byte/seg
 */
#define SEG_ADDR 0xD400 //第一个段
#define SEG_NUM  5      //一共几个段。512byte*5=2.5K
#define SEG_ADDR_END 0xDFFF

#define TEMP_SEG_ADDR 0xE000//0xD000-0xE5FF

//#define BACKUP_DATA_START 0xE200 //9720/12=810帧，够2-3个星期最大的量

 void wrt_flash_arry(uint16_t addr,uint8_t* data,uint8_t len);
 void erase_area();
 void erase_Seg(uint16_t segAddr);
 void wrt_flash_byte(uint16_t addr,uint8_t data);

#endif /* FLASH_EEPROM_H_ */