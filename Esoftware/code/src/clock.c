#include "clock.h"
#include "core.h"
#include <driverlib.h>
static void select_xt1(void);
static void dco_config(void);

void clock_config(void)
{
    WDTCTL = WDTPW + WDTHOLD;                   // 停止看门狗
    
    //关闭usb,达到最低功耗。
    USBKEYPID = USBKEY;
    USBPWRCTL = 0;
    USBKEYPID = 0;

    __delay_cycles(250000);
    //UCS_LFXT1Start(,UCS_XT1_DRIVE3,UCS_XCAP_3);
    select_xt1();              // 选择XT1
    dco_config();             // ACLK = XT1 = 32.768K MCLK = SMCLK = 8000K
}

static void select_xt1(void)
{
    // Set up XT1
    P5SEL |= BIT4+BIT5;                       // Select XT1
    UCSCTL6 &= ~(XT1OFF);                     // XT1 On
    UCSCTL6 |= XCAP_3;                        // Internal load cap 
    UCSCTL6 |= XT1DRIVE_3;
    //UCSCTL6 &= ~(0x0020u);                    //LOW fRE MODE
    do
    {
        UCSCTL7 &= ~XT1LFOFFG;                  // 清楚XT1错误标记
    }while (UCSCTL7&XT1LFOFFG);                 // 检测XT1错误标记

}

static void dco_config(void)
{
    __bis_SR_register(SCG0);    // Disable the FLL control loop	禁止FLL功能
    UCSCTL0 = 0x0000;           // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;        // Select DCO range 16MHz operation
    UCSCTL2 = FLLD_1 + 243;     // Set DCO Multiplier for 8MHz
                                // (N + 1) * FLLRef = Fdco
                                // (243 + 1) * 32768 = 8MHz         计算公式
                                // Set FLL Div = fDCOCLK/2
    __bic_SR_register(SCG0);    // Enable the FLL control loop	使能FLL功能

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 8 MHz / 32,768 Hz = 250000 = MCLK cycles for DCO to settle
    __delay_cycles(250000);

    // Loop until XT1,XT2 & DCO fault flag is cleared
    do
    {
       // UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
                               // Clear XT2,XT1,DCO fault flags	清除所有错误标志位
        SFRIFG1 &= ~OFIFG;     // Clear fault flags		清除振荡器错误
    }while (SFRIFG1&OFIFG);   // Test oscillator fault flag    等待
}
/*初始化RTC*/
void initRTC(){
  uint8_t flag = 0;
  union UN_VALUE {
    Calendar TimeNow;
    uint8_t valByte[8];
  }un_Calendar;
  
  RTC_A_holdClock(RTC_A_BASE);
  RTC_A_disableInterrupt(RTC_A_BASE,RTCAIE);
  RTC_A_disableInterrupt(RTC_A_BASE,RTCTEVIE);
  RTC_A_disableInterrupt(RTC_A_BASE,RTCRDYIE);
  
  M24C64_ReadPageWithLen(BK_TIME_ADDR,un_Calendar.valByte,8);
  flag = (*((uint8_t *)POWER_ON_ADDR));
  if((POWER_VERF != flag)||
     (un_Calendar.TimeNow.DayOfWeek>7)||
     (un_Calendar.TimeNow.Month>31)){//未激活或eeprom时间格式不对
       
    un_Calendar.TimeNow.Year       = NOW_YEAR;//2014年
    un_Calendar.TimeNow.Month      = NOW_Y;//10月
    un_Calendar.TimeNow.DayOfMonth = NOW_D;//1号
    un_Calendar.TimeNow.DayOfWeek  = NOW_DOW;//星期2
    un_Calendar.TimeNow.Hours      = NOW_H;//12点
    un_Calendar.TimeNow.Minutes    = NOW_M;//12分
    un_Calendar.TimeNow.Seconds    = NOW_S;//12秒
  }
   
  g_old_date = un_Calendar.TimeNow.DayOfMonth;
  RTC_A_calendarInit(RTC_A_BASE,
                       un_Calendar.TimeNow,
                       RTC_A_FORMAT_BINARY);
 
    
    //设置每分钟发生一次中断。
    RTC_A_setCalendarEvent(RTC_A_BASE,
                RTC_A_CALENDAREVENT_MINUTECHANGE);
    //设置闹钟 22:30
    RTC_A_setCalendarAlarm(RTC_A_BASE,SLEEP_TIME_M,SLEEP_TIME_H,//22:00
                           RTC_A_ALARMCONDITION_OFF,//零点要触发闹钟。
                           RTC_A_ALARMCONDITION_OFF);

    //使能闹钟中断。
    RTC_A_enableInterrupt(RTC_A_BASE,RTCAIE);

    //Start RTC Clock
    RTC_A_startClock(RTC_A_BASE);
}
/*时间备份*/
void TimeBackup(){
    
    union UN_VALUE {
      Calendar TimeNow;
      uint8_t valByte[8];
    }un_Calendar;
    
    un_Calendar.TimeNow = RTC_A_getCalendarTime(RTC_A_BASE);
    if(un_Calendar.TimeNow.Minutes != 59)
      un_Calendar.TimeNow.Minutes += 1;
    M24C64_WritePageWithLen(BK_TIME_ADDR,un_Calendar.valByte,8);
    
}
void dayChange(){
  
    Calendar TimeNow;
    TimeNow = RTC_A_getCalendarTime(RTC_A_BASE);
    if((TimeNow.Hours==23)&&(TimeNow.Minutes>=59)){
        g_old_date = TimeNow.DayOfMonth;
    }
    if((TimeNow.Hours==0)&&(TimeNow.Minutes<=3)){//零点,运动量标识符更新
      ALLSTEPS = 0;
      //计步状态跨零点重建标识符
      if((DAY_MODE == CurrentData.Mode)&&(TimeNow.Minutes==0)){
          WriteCurrentDataToEEPROM();
          init_CurrentData(CurrentData.Mode);
          creatMotionFlag();
      }
    }
}
