#include <msp430.h>
#include "ActRainbow.h"
#include "core.h"
#include "FLASH_EEPROM.h"
#include "led.h"
#include "clock.h"
#include "interrupt_extern.h"


uint8_t standstill = 0;
uint16_t sleepTime =0;
uint16_t deepSleepTime=0;
uint8_t unActiveMinutes = 0;
//秒中断处理函数
void sec_interrupt_handle(){
  
         if(FLAG_WirlessCharge == event_flag){          //如果是充电状态
            if(GPIO_getInputPinValue(GPIO_PORT_P1,
                GPIO_PIN4)==GPIO_INPUT_PIN_LOW){        //确定还在充电
                  
              sec_clock++;
              if((sec_clock%(4))==1){                         //做充电灯光显示
                if(g_powerlever < 12){
                  //clock_light_color_AllOff();
                  for(uint8_t i =1;i<=g_powerlever;i++){
                    clock_light_on(i);
                    delay_ms(100);
                  }
                  delay_ms(500);
                  clock_light_color_AllOff();
                  //ledShow();
                }
                else{                                   //充满了

                  for(uint8_t i =0;i<2;i++){
                    clock_light_color_AllOn();
                    delay_ms(200);
                    clock_light_color_AllOff();
                    delay_ms(100);
                  }
                }
              }
            }
            else{                                       //不在充电状态
              unActiveMinutes = 0;
              TA0CCTL0 &= ~CCIE;                         //关中断
              g_pwm_show = false;
              timer_1ms_couter = 0;
              clock_light_color_AllOff();               //关闭灯光
              RTC_A_disableInterrupt(RTC_A_BASE,RTCRDYIE);
              ActiveWorkMode();                         //进入正常工作模式
              if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
                creatMotionFlag();
                init_CurrentData(DAY_MODE);             //新建数据结构
                montion_lever_1m[0]=199;
                montion_lever_1m[1]=199;
              }
              event_flag = FLAG_Nothing;                //清除标志位
            }
          }
}

/*分钟中断处理函数*/
void min_interrupt_handle(){
  
uint16_t montion_Value;
uint16_t montion_Value_Avr;
uint16_t montion_Value_Avr_5m=0;
  
#ifdef DEBUG_LIGHT          
          if(g_SleepDetect == ActiveSleepDetectOpen){
            RGB_Light_ON(LEDGREEN);
          }
          else{
             RGB_Light_ON(LEDRED);
          }
#endif
          /*未激活，无充电则进入低功耗状态*/
          if(POWER_VERF != (*((uint8_t *)POWER_ON_ADDR))){
            if(FLAG_WirlessCharge != event_flag){
              unActiveMinutes++;
              if(unActiveMinutes>=5){
                unActiveMinutes = 0;
                unActiveMode();
                event_flag = FLAG_intoStandby;
              }
            }
          }else{
            dayChange();
          }
          /*如果是在无线充电模式下*/
          if(FLAG_WirlessCharge == event_flag)
          {
             getBatterVolte();          //一分钟获取一次最新的电压值
             led_charging();            //更新相应电压的灯光指示
          }
          /*正常工作模式下*/
          else if((FLAG_intoStandby != event_flag)&&
                  (POWER_VERF == (*((uint8_t *)POWER_ON_ADDR)))){
             if(DAY_MODE == CurrentData.Mode)
                  montion_lever_1m[0] = montion_lever_1m[0]/3;
             if(montion_lever_1m[0]<=1){
                standstill++;
                if(standstill>=TIME_TO_STANDBY_MODE){
                  /*大于1.5个小时不动弹，进入待机状态*/
                  event_flag = FLAG_intoStandby;
                 
                    if(NIGHT_MODE == CurrentData.Mode){
                      if((sleepTime<=(deepSleepTime+5))||//不动时间太多
                         (sleepTime<=standstill))//睡眠时间不能小于standstill
                        {
                          standstill=110;
                        }
                    }
                    if(standstill!=110){//现在的数据保存好。
                         dataReshape();
                         WriteCurrentDataToEEPROM();
                    }
                  
                  intoStandbyMode();
                  standstill = 0;
#ifdef DEBUG_LIGHT                   
                  RGB_Light_OFF(LEDGREEN);
                  RGB_Light_OFF(LEDRED);
#endif                    
                  return;
                }
            }else{
              standstill=0;
            }
            //=================================================================
            /*如果已经进入了睡眠状态*/
            if(NIGHT_MODE == CurrentData.Mode){
              montion_Value_Avr = 0;
              for(uint8_t i=0;i<5;i++){//计算5分钟的运动量。
                montion_Value_Avr += montion_lever_1m[i];
              }
              if((montion_lever_1m[0]< MAX_MOTION_OF_SLEEP)&&//判断是否低于最大运动量
                 ((montion_Value_Avr/5) <  MOTION_OUTTO_SLEEP))//平均低于MOTION_OUTTO_SLEEP
              {
                 UpdataCurrentData(montion_lever_1m[0]);
                 if(montion_lever_1m[0]<=1) deepSleepTime++;//不动加1
                 sleepTime++;//睡眠时间加1
              }else{
              //大于最大运动量，则唤醒。
                //这里的唤醒是真正的起床走动，与睡眠里面的清醒不是一个概念。
                
                /*是否是有效睡眠的判断___排除静止*/
                if((sleepTime>(deepSleepTime*8)/10)&&//深度睡眠时间不能太多。
                   (sleepTime>(standstill+5))&&//睡眠总时间要比清醒前不动时间多5分钟
                     (sleepTime>60))//睡眠时间不能短于60分钟
                {
                  montion_Value =0;
                }else{
                  montion_Value ++;
                }
                
                /*是否是有效睡眠的判断___排除静坐*/
                if(sleepTime<120){
                  montion_Value_Avr = 0;
                  for(uint8_t i=0;i<TIME_INTO_SLEEP;i++){//计算最近40分钟的评价运动量
                        montion_Value_Avr += montion_lever_1m[i];
                  }
                  if((montion_Value_Avr/TIME_INTO_SLEEP) >=8)
                      montion_Value++;
                }
                
                if(montion_Value==0){
                  UpdataCurrentData(100);
                  dataReshape();
                  WriteCurrentDataToEEPROM();
                }
                init_CurrentData(DAY_MODE);
                ActiveWorkMode();
                creatMotionFlag();
              }
            }
            else
            {/*如果未进入睡眠状态*/
              if(g_SleepDetect == ActiveSleepDetectOpen)
              {
              //判断何时可以进入睡眠状态。
                montion_Value = 0;
                montion_Value_Avr = 0;
                //=====================
                //加入步数判断，如果步数>N  montion_Value++;
                //======================
                for(uint8_t i=(TIME_INTO_SLEEP-MIN_TIME_INTO_SLEEP);i<TIME_INTO_SLEEP;i++){//持续的时间
                  if(montion_lever_1m[i] > MIN_MOTION_TO_SLEEP){
                    //持续的运动量水平低于MIN_MOTION_TO_SLEEP
                    montion_Value++;
                  }
                  if(i%5==0){
                    /*计算5分钟的运动量不大于MIN_MOTION_TO_SLEEP_5M*/
                    montion_Value_Avr_5m = 0;
                    for(uint8_t j=0;j<5;j++){
                      montion_Value_Avr_5m += montion_lever_1m[i+j];
                    }
                    if(montion_Value_Avr_5m>=MIN_MOTION_TO_SLEEP_5M) 
                       montion_Value++;
                  }
                  montion_Value_Avr += montion_lever_1m[i];
                }
                
                /*在持续的运动量低于某个水平的基础上，还要加上平均的运动量低于某个水平*/
                if((montion_Value_Avr/MIN_TIME_INTO_SLEEP) > AVR_MOTION_TO_SLEEP)
                  montion_Value++;
                if(0==montion_Value)
                {
                  dataReshape();
                  WriteCurrentDataToEEPROM();
                  //切换到睡眠模式，灯光效果。
                  init_CurrentData(NIGHT_MODE);
                  SleepMonitorMode();
                  sleepTime = 0;
                  deepSleepTime = 0;
                  for(uint8_t i=1;i<TIME_INTO_SLEEP;i++)
                  {//将之前的睡眠数据导入。
                    UpdataCurrentData(montion_lever_1m[TIME_INTO_SLEEP-i]);
                    CurrentData.minutes++;
                    if(montion_lever_1m[TIME_INTO_SLEEP-i]<=1) deepSleepTime++;//不动加1
                    sleepTime++;//睡眠时间加1
                    if((MINUTES_CYCLE)== CurrentData.minutes){
                        CurrentData.minutes = 0;
                        dataReshape();
                    }
                  }
                }
              }
            }
            //=======================================================================
            CurrentData.minutes++;//分钟数递增
            /*运动量数据输出*/
            if(DAY_MODE == CurrentData.Mode){//白天模式才有运动量
              G_montion_Value += montion_lever_1m[0];
              if((CurrentData.minutes)%5==0){
                  /*===========更新运动量================*/
                  G_montion_Value = G_montion_Value/3;
                  addMotionValueToEEPROM(G_montion_Value);
                  G_montion_Value = 0;
              }
            }
            /*如果满了一个输出周期*/ 
            if((MINUTES_CYCLE)== CurrentData.minutes)
            {
                CurrentData.minutes = 0;
                dataReshape();
                if(250 <= CurrentData.ZeroNum){
                    WriteCurrentDataToEEPROM();
                    init_CurrentData(CurrentData.Mode);
                }
                if( 250 <= CurrentData.DataLen){
                  WriteCurrentDataToEEPROM();
                  init_CurrentData(CurrentData.Mode);
                }
                /*获取电压，如果电压太低，就进入待机模式，罢工*/
               getBatterVolte(); 
                if(AD_value <= LOW_VOLTAGE_TO_ALM){
                  TimeBackup();
                } 
                if(AD_value <= LOW_VOLTAGE_TO_SBM){
                  event_flag = FLAG_intoStandby;
                  //现在的数据保存好。
                  dataReshape();
                  WriteCurrentDataToEEPROM();
                  intoStandbyMode();
                  //return;
                }
            }
            /*运动量数组压栈*/
            for(uint8_t i=0;i<TIME_INTO_SLEEP-1;i++){
              montion_lever_1m[TIME_INTO_SLEEP-1-i] = 
                              montion_lever_1m[TIME_INTO_SLEEP-i-2];
            }
            montion_lever_1m[0] = 0;
          }
#ifdef DEBUG_LIGHT           
          RGB_Light_OFF(LEDGREEN);
          RGB_Light_OFF(LEDRED);
#endif

}

//闹钟中断处理函数
void alm_interrupt_handle(){
  
    Calendar TimeNow;
    TimeNow = RTC_A_getCalendarTime(RTC_A_BASE);
          if(TimeNow.Hours>=SLEEP_TIME_H){
              g_SleepDetect = ActiveSleepDetectOpen;
              RTC_A_setCalendarAlarm(RTC_A_BASE,WAKEUP_TIME_M,WAKEUP_TIME_H,//8:15
                           RTC_A_ALARMCONDITION_OFF,
                           RTC_A_ALARMCONDITION_OFF);
         }
          else{
              g_SleepDetect = ActiveSleepDetectClose;
              RTC_A_setCalendarAlarm(RTC_A_BASE,SLEEP_TIME_M,SLEEP_TIME_H,//22:10
                           RTC_A_ALARMCONDITION_OFF,
                           RTC_A_ALARMCONDITION_OFF);
         }

}
