
#include "bsp.h"
#include "core.h"
#include "m24c64.h"
#include "led.h"
#include "uart1.h"
#include "FLASH_EEPROM.H"
#include "inc/hw_regaccess.h"

void i2c_config()
{
  P4SEL &= ~BIT2;                        // P3.2@UCB1SCL
  P4DIR |= BIT2;
  P4OUT |= BIT2;
  // 输出9个时钟 以恢复I2C总线状态
  for( uint8_t i = 0 ; i < 9 ; i++ )
  {
    P4OUT |= BIT2;
    __delay_cycles(8000);
    P4OUT &= ~BIT2;
    __delay_cycles(8000);
  }
  
  P4SEL |= (BIT1 + BIT2);                  // P4.1-->SDA ，P4.2 -->SCL
  
  UCB1CTL1 |= UCSWRST;
  UCB1CTL0 = UCMST + UCMODE_3 + UCSYNC ;  // I2C主机模式
  UCB1CTL1 |= UCSSEL_2;                   // 选择SMCLK
  UCB1BR0 = 20;                           //400--8M/400=200K
  UCB1BR1 = 0;                            //200--8M/200=400K
  UCB1CTL0 &= ~UCSLA10;                   // 7位地址模式
  //UCB1I2CSA = EEPROM_ADDRESS;            // 这个地址放在读写里面去定义，以便选择不同的I2C器件
  UCB1CTL1 &= ~UCSWRST;
}


//void assert_failed(uint8_t* file, uint16_t line)
//{ 
//  // 输出错误信息
//  printf(" file %s on line %d\r\n", file , line);
//  while (1)
//  {
//  }
//}

/*
初始化IO口
*/
void uart4out(){
      GPIO_setAsOutputPin(UART_PORT,
                         UART_RX_PIN+UART_TX_PIN);
      
      GPIO_setOutputLowOnPin(UART_PORT,
                         UART_RX_PIN+UART_TX_PIN);

}

void port_Init(){
  
    /*=========PORT6  RGB PIN====P6.0--R,P6.1--G,P6.2--B==========*/
      //设置RGB I/O口为输出。
    GPIO_setAsOutputPin( LED_RGB_PORT,
                         LED_R_PIN+LED_G_PIN+LED_B_PIN);
    
    RGB_Light_AllOFF();   
    /*==========================
   PORT1.0 LIS3DH INT2 PIN 
   PORT1.1  LIS3DH INT1
   PORT1.2  FLASH LIGHT PHOTODIDO PIN
   PORT1.3  DATA LIGHT PHOTODIDO PIN
   */
    
    GPIO_setAsInputPinWithPullDownresistor(GPIO_PORT_P1,
                                          LIS3DH_INT1_PIN);
    GPIO_setAsInputPin(GPIO_PORT_P1,
                      FLASH_LIGHT_PIN+DATA_LIGHT_PIN);
    
     //上升沿中断
    GPIO_interruptEdgeSelect(GPIO_PORT_P1,
                            //LIS3DH_INT2_PIN+
                            LIS3DH_INT1_PIN+
                            FLASH_LIGHT_PIN,
                            //DATA_LIGHT_PIN,
                            GPIO_LOW_TO_HIGH_TRANSITION);
    
    /*=============PORT2.0 V_EEPROM =======================*/
      GPIO_setAsOutputPin( V_EEPROM_PROT,
                           V_EEPROM_PIN);
      GPIO_setOutputLowOnPin(V_EEPROM_PROT,
                              V_EEPROM_PIN);
    
    /*============PORT J 屏幕通讯光电二极管电源控制教========*/
      GPIO_setAsOutputPin( V_DATA_PHOTODIODE_PORT,
                             V_DATA_PHOTODIODE_PIN);
      //GPIO_setOutputHighOnPin(V_DATA_PHOTODIODE_PORT,
      //                      V_DATA_PHOTODIODE_PIN);//V_D电源开
      
      GPIO_setOutputLowOnPin(V_DATA_PHOTODIODE_PORT,
                           V_DATA_PHOTODIODE_PIN);//电源关闭
      
      /*============VCHAG 充电信号检测========*/
      GPIO_setAsInputPin( V_CHG_PORT,
                         V_CHG_PIN); 
      //上拉输入
      GPIO_setAsInputPinWithPullUpresistor(V_CHG_PORT, V_CHG_PIN);
      
      //下降沿中断
      GPIO_interruptEdgeSelect(V_CHG_PORT,
                               V_CHG_PIN,
                               GPIO_HIGH_TO_LOW_TRANSITION);
      //中断使能
      GPIO_enableInterrupt(V_CHG_PORT,
                           V_CHG_PIN);
      //清除中断标志位。
      GPIO_clearInterruptFlag(V_CHG_PORT,
                              V_CHG_PIN);
      
      /*=============四周绿光===========================*/
   GPIO_setAsOutputPin(
                    CLOCK_LED_PORT5,
                    CLOCK_LED_PIN5 );
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT6,
                    CLOCK_LED_PIN6 );
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT7,
                    CLOCK_LED_PIN7 );
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT8,
                    CLOCK_LED_PIN8 );
        
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT1,
                    CLOCK_LED_PIN1
                    ); 
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT2,
                    CLOCK_LED_PIN2
                    ); 
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT3,
                    CLOCK_LED_PIN3
                    ); 
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT4,
                    CLOCK_LED_PIN4
                    );
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT9,
                    CLOCK_LED_PIN9
                    );
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT10,
                    CLOCK_LED_PIN10
                    );
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT11,
                    CLOCK_LED_PIN11
                    );
        GPIO_setAsOutputPin(
                    CLOCK_LED_PORT12,
                    CLOCK_LED_PIN12
                    );
        clock_light_color_AllOff();
        /*========分压电路(地)开关=========================*/
        GPIO_setAsOutputPin(
              G_AD_VBAT_PORT,
              G_AD_VBAT_PIN
              );
        GPIO_setOutputHighOnPin(
             G_AD_VBAT_PORT,
             G_AD_VBAT_PIN
             );
        //============UART做输出=============================
      /*   GPIO_setAsOutputPin( GPIO_PORT_P4,
                         BIT5); 
         GPIO_setOutputLowOnPin(
                    GPIO_PORT_P4,
                    BIT5);
       */

}

void self_test(){
  
  uint8_t j,vale_data,eeprom_data[10] = {0,1,2,3,4,5,6,7,8,9};
    // 绿红灯1次,表示开始自检
  for(j=0;j<2;j++){
    P6OUT ^= BIT1;
    delay_ms(1000);
  }
    //呼叫LIS3DH，看看器件是否正常
    if(!(LIS3DH_GetWHO_AM_I(&vale_data))){
       //器件不正常
       // uart1_sendbuf("LIS3DH Can't be read");
        
        //红灯亮两下代表读加速度计失败
        for(j=0;j<4;j++){
          P6OUT ^= BIT0;
          delay_ms(1000);
        }
        LPM3;
    }
    else{    
      if(0x33 != vale_data){
          //器件不正常
         // uart1_sendbuf("LIS3DH ID ERR");
          
          //红灯亮两下代表读加速度计失败
          for(j=0;j<4;j++){
            P6OUT ^= BIT0;
            delay_ms(1000);
          }
          LPM3;
       }      
    }    
     
  //写EEPROM
   M24C64_WritePageWithLen(TEST_PAGE_ADDR,eeprom_data,10);
   delay_ms(10);//写时间5ms
  
    for(j=0;j<10;j++){
        eeprom_data[j] = 0;    
    }
   //读EEPROM
   M24C64_ReadPageWithLen(TEST_PAGE_ADDR,eeprom_data,10);
   
   
   for(j=0;j<10;j++){
     if(j != eeprom_data[j]){
        //器件不正常
      //  uart1_sendbuf("EEPROM verfity false");
        for(j=0;j<4;j++){
          P6OUT ^= BIT0;
          delay_ms(1000);
        }
        LPM3;
     }
   
   }

  //自检通过，亮绿灯
  for(j=0;j<2;j++){
    P6OUT ^= BIT1;
    delay_ms(1000);
  }
}

void timerA_init(){
  
    //TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
    TA1CCR0 = FLASH_LIGHT_COMPARE_VALUE-1;      //20us                   
    TA1CTL = TASSEL_2 + MC_0 + TACLR ;        // SMCLK, stopmode, clear TAR
    
    //TA0CCTL0 = CCIE;                        // CCR0 interrupt enabled
    TA0CCR0 = LCD_DATA_COMPARE_VALUE-1;       //1MS                   
    TA0CTL = TASSEL_1 + MC_1 + TACLR ;        // ACLK, upmode, clear TAR
    
    //关闭bootloader中使用的定时器
    TA2CTL = TASSEL_1 + MC_0 + TACLR ;        // smCLK, upmode, clear TAR
    
}
void timerB_init(){
    //TBCCTL0 = CCIE;                          // CCR0 interrupt enabled
    TBCCR0 = RGBW_LIGHT_COMPARE_VALUE-1;        //270MS                   
    TBCTL = TBSSEL_1 + MC_1 + TBCLR ;           // ACLK, upmode, clear TBR
}
/*
硬件初始化
*/
void SYS_INIT(void){
  
  //初始化XT1 SMCLK 8MHz
  clock_config();
  
  //IO口，i2c初始化。
  port_Init();
    
  i2c_config();
  
  //初始化RTC
  initRTC();

  //初始化UART1。BR == 9600
#ifdef UART_ON
  uart1_config();
#else
  //uart4out();
#endif
  //加速度计、EEPROM、灯光自检  
  self_test();
  //初始化环境变量
  init_EnvironmentVariable();  
  timerA_init();
  timerB_init();
}

/*
初始化环境变量

全局变量存于ROM中，以防止掉电，比如BlockInfo定义了EEPROM的使用情况

*/
void init_EnvironmentVariable(void){
  uint8_t flag = 0;
  uint8_t tempbyte=0;
  flag = (*((uint8_t *)POWER_ON_ADDR));
  if(POWER_VERF != flag){//第一次开机
    //eeprom指针
    BlockInfo.CurrentBlockAdrress = FRIST_DATA_BLOCK_ADDR;
    BlockInfo.DataBlockNum = 0;
    BlockInfo.CutAdrress = 0;
    CurrentData.Mode = FIRST_MODE;
    CurrentData.ZeroZip = false;
    motionAddress = MOTION_PAGE_ADDR;
    motionCutAddress = 0;
    updataBlockInfoToEEPROM();
    updataMotionAddressToEEPROM();
    //设置目标
    TagetStep = 8000;
    M24C64_WriteByte(TAGET_ADDR,0x1F);
    M24C64_WriteByte(TAGET_ADDR+1,0x40);
    //修改开机标志位
    //M24C64_WriteByte(FRIST_POWER_ON_ADDR,POWER_VERF);
    //wrt_flash_byte(POWER_ON_ADDR, POWER_VERF);
  }
  else{
    reakBlockInfoFromEEPROM();
    reakMotionAddressFromEEPROM();
    creatMotionFlag();
    M24C64_ReadByte(TAGET_ADDR,&tempbyte);
    TagetStep = tempbyte<<8;
    M24C64_ReadByte(TAGET_ADDR+1,&tempbyte);
    TagetStep += (tempbyte &0xff);
  }
  init_CurrentData(FIRST_MODE);
  M24C64_WriteByte(LOG_PAGE_ADDR,logAddress>>8);
  M24C64_WriteByte(LOG_PAGE_ADDR+1,logAddress);
  M24C64_WriteByte(LOG_PAGE_ADDR+2,0xff);
  M24C64_WriteByte(LOG_PAGE_ADDR+3,0xff);
  
//  for(uint8_t i=0;i<TIME_INTO_SLEEP;i++){
//    montion_lever_1m[i]=190;
//  }
  //montion_lever_1m[0]=199;
  //montion_lever_1m[1]=199;
  montion_lever_1m[0]=1;
  montion_lever_1m[1]=0;
}//测试ok


void getBatterVolte(){
    //使能分压电路
    GPIO_setOutputLowOnPin(
         GPIO_PORT_PJ,
         GPIO_PIN1
         );
    //Initialize the ADC10_A Module
    /*
     * Base Address for the ADC10_A Module
     * Use internal ADC10_A bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    
    ADC10_A_init(ADC10_A_BASE,
        ADC10_A_SAMPLEHOLDSOURCE_SC,
        ADC10_A_CLOCKSOURCE_ADC10OSC,
        ADC10_A_CLOCKDIVIDER_1);
    
    ADC10_A_enable(ADC10_A_BASE);
    
    /*
     * Base Address for the ADC10_A Module
     * Sample/hold for 16 clock cycles
     * Do not enable Multiple Sampling
     */
    ADC10_A_setupSamplingTimer(ADC10_A_BASE,
        ADC10_A_CYCLEHOLD_64_CYCLES,
        ADC10_A_MULTIPLESAMPLESDISABLE);
   //Configure Memory Buffer
    /*
     * Base Address for the ADC10_A Module
     * Use input A4，如果修改AD脚，修改此处
     * Use positive reference of Internally generated Vref
     * Use negative reference of AVss
     */
    ADC10_A_memoryConfigure(ADC10_A_BASE,
        ADC10_A_INPUT_A4,
        ADC10_A_VREFPOS_INT,
        ADC10_A_VREFNEG_AVSS);
    
     //Configure internal reference
    //If ref generator busy, WAIT
    while (REF_BUSY == REF_isRefGenBusy(REF_BASE));
    //Select internal ref = 2.5V
    REF_setReferenceVoltage(REF_BASE,
    		REF_VREF2_5V);
    //Internal Reference ON
    REF_enableReferenceVoltage(REF_BASE);
    //Delay (~75us) for Ref to settle
    __delay_cycles(800);
    //Enable and Start the conversion
    //in Single-Channel, Single-Conversion Mode
    ADC10_A_startConversion(ADC10_A_BASE,
        ADC10_A_SINGLECHANNEL);
    //ADC10BUSY?
    while (ADC10_A_isBusy(ADC10_A_BASE));
    
    AD_value = (ADC10_A_getResults(ADC10_A_BASE)/1024.0f)*5.0f;
    //__delay_cycles(800);
    
        //关闭分压电路    
    GPIO_setOutputHighOnPin(
     GPIO_PORT_PJ,
     GPIO_PIN1
     );

    
//    ADC10_A_disableConversions(ADC10_A_BASE,
//                    ADC10_A_PREEMPTCONVERSION);
    
    REF_disableReferenceVoltage(REF_BASE);   
    ADC10_A_disableSamplingTimer(ADC10_A_BASE);
    ADC10_A_disable(ADC10_A_BASE);
}













