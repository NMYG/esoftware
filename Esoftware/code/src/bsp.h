#ifndef __BSP__H
#define __BSP__H

#include "ActRainbow.h"
//定时器相关定义
#define RGBW_LIGHT_COMPARE_VALUE    10    // 1/32768*9=270us 10-->305us 
#define LCD_DATA_COMPARE_VALUE      30    // 1/32768*30=1MS
#define FLASH_LIGHT_COMPARE_VALUE   160  // 1/8M*8=20uS


#define POWER_ON_ADDR             0xF000  //开机标志位
#define POWER_VERF  0x56
//EEPROM相关定义
#define TEST_PAGE_ADDR                  0x0000  //1<<5   //测试区块

//#define FRIST_POWER_ON_ADDR             0x0020  //开机标志位

#define FRIST_BLOCK_INFO_ADDR           (1<<7)  //数据信息块开始地址.
#define TAGET_ADDR                      (3<<7)  //目标
//#define BK_TIME_ADDR                  (5<<7)  //时间备份
#define FRIST_DATA_BLOCK_ADDR           (10<<7)  //第一个数据块地址 200*128=25K

#define MOTION_PAGE_ADDR                (200<<7) //运动量存储区域200~300 40*128=5K
#define LOG_PAGE_ADDR                   (300<<7) //LOG存储区域210~250 10*128=1.28k
#define LAST_DATA_BLOCK_ADDR            (490<<7)  //一共512个page，最大用到490
#define PAGE_LEN  (1<<7)
//共240个page做为数据存储区，共240*32空间。
//

 void assert_failed(uint8_t* file, uint16_t line);
 void port_Init(void);
 void board_config(void);
 void i2c_config(void);
 void self_test(void);
 void SYS_INIT(void);
 uint8_t FlashLightCount(void);
 void init_EnvironmentVariable(void);
 void getBatterVolte();//获取电池电压及电量百分比。
#endif