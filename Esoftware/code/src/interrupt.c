/*
 * interrupt.c
 *
 *  Created on: 2013-9-23
 *      Author: Xiezuyong
 */
#include <msp430.h>
#include <stdint.h>
#include "ActRainbow.h"
#include "Encode.h"
#include "core.h"
#include "FLASH_EEPROM.h"
#include "led.h"
#include "clock.h"
#include "interrupt_extern.h"
bool g_pwm_on = false;
bool g_pwm_show = false;
uint8_t g_rgbLED_pwm = LED_G_PIN;
uint8_t headerLED = 1;
volatile uint16_t timeB_couter = 0;                //timeB计数
volatile uint16_t timeB_again_couter = 0;          //重发计数
volatile uint16_t reSend_couter = 0;               //重发次数
volatile uint8_t  flash_couter = 0;                //闪光灯计数
volatile uint16_t timer_1ms_couter = 0;            //1毫秒定时计数
volatile uint16_t timer_1ms_TimeUp = 0;            //1000毫秒超时
volatile uint16_t timer_20us_couter = 0;
volatile uint8_t RTC_1s_couter = 0;                //1秒定时计数
volatile MY_FLAG flashCoutingFlag = MY_NO;      //闪光灯计数模式开启
volatile uint8_t Flash_flag = 0;

uint8_t puse_arry[12]={0};
char *Flash_ptr= (char *)SEG_ADDR;

char ScreenHeaderFlag =MY_NO;

uint8_t color,bit,bytedata;

int val_j = 0,val_k = 0,val_i = 0;              //中断里面的循环要放全局中断
uint8_t screenByte[4];                             //接收到的屏幕光的数据
uint16_t bitCounter=0;

// Timer1 A0 interrupt service routine
/*处理闪关灯频率的计算*/
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
    timer_20us_couter ++;
    if(timer_20us_couter>=3000){//20*2000=80ms
      TA1CCTL0 &= ~CCIE;
      timer_20us_couter = 0;
      GPIO_disableInterrupt(GPIO_PORT_P1,       //关外部中断
                          FLASH_LIGHT_PIN);
      LPM3_EXIT;
    }
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
{
   //这个定时器用作毫秒级别的定时
  timer_1ms_couter++;
  /*电量显示 pwm1/6 */
  if(g_pwm_on){
    if(timer_1ms_couter%6==1){
      P6OUT |=0x07;                              //全灭
    }
    else if(timer_1ms_couter%6==0){
      P6OUT &= (~g_rgbLED_pwm);                  //亮某色
    }
    if(timer_1ms_couter>=1200){                   //电量只显示半秒。
      P6OUT |=0x07;                              //全灭
      TA0CCTL0 &= ~CCIE;                         //关中断
      g_pwm_on = false;
      timer_1ms_couter = 0;
    }
  }
  /*炫彩效果灯*/
  else if(g_pwm_show){
    ledShow_pwm();
    //chargeLED_pwm();
  }else{
    /*屏幕光通信*/
     timer_1ms_TimeUp++;
     if((timer_1ms_couter>=1000)||(timer_1ms_TimeUp>=5000)){
        dataReceiveOff();                         //两秒没数据退出；
        LPM3_EXIT;
      }
  }
}


/*处理彩光发送*/
#pragma vector=TIMERB0_VECTOR
__interrupt void TIMERB0_ISR (void)
{
  //先发送6秒的曝光用数据
  if(timeB_couter<20000){
    timeB_couter++;
    if(timeB_couter == 20000){
      val_i = 0;
      val_j = 0;
      val_k = 0;
      bit = 0;
      timeB_again_couter = 0;
    }
  }
  else{
    /*现在再正式发送送数据*/
    if(val_k < reSend_couter){
      /*少于5秒的发5秒，大于5秒的发3轮。*/
      if(val_i<2){
          P6OUT |=0x07;
          P6OUT &= 0xfb;                        //蓝色条纹
      }
      else{
          if(bit==0)
             bytedata = *Flash_ptr++;
          color = (bytedata <<(bit*2)) & 0xC0;
          if(3==bit++)
            bit = 0;
          switch (color){
            case 0x00:
                color=WHITE;
                break;              
            case 0x40:           
                color=RED;
                break;             
            case 0x80:          
                color=GREEN;
                break;
            case 0xC0:
                color=BLACK;
                break;
           }
          P6OUT |=0x07;
          P6OUT &= (~color);
      }
      val_i++;
      if(2 > timeB_again_couter){
        if(val_i==(BAND_NUM+2)){                //第1/2遍发送完毕
             val_i = 0;
             Flash_ptr-=BYTE_NUM;               //指针回到BYTE_NUM字节之前。
             timeB_again_couter ++;
        }
      }else{
        if(val_i== (BAND_NUM+2)){               //第3遍发送完毕
             val_i = 0;                         //比特指针归零
             timeB_again_couter = 0;            //一石二鸟：1，else 0
             val_j++;                           //帧指针递加
        }
      }
      if(val_j == seg){
        val_j = 0;
        Flash_ptr = (char *)SEG_ADDR;           //清空变量
        timeB_again_couter = 0;
        val_i = 0;
        bit = 0;
        val_k++;
      }
    }
    else{
        LPM3_EXIT;
    }
  }

}

/*外部中断处理函数*/
#pragma vector = PORT1_VECTOR
__interrupt void port1_ISR(void)
{
  //uint8_t statu = 0;
  switch(P1IV)
  {
    //LIS3DH interupt
    
   case P1IV_P1IFG0:
       break;
    
    case P1IV_P1IFG1:                           //加速度计中断
      
      if(event_flag == FLAG_intoStandby){
          RTC_A_clearInterrupt(RTC_A_BASE,
                            RTC_A_TIME_EVENT_INTERRUPT);
          event_flag = FLAG_OutOfStandby;
          unActiveMinutes = 0;
          LPM3_EXIT;
      }
      else if(event_flag == FLAG_Nothing){
          //LIS3DH_GetFifoSourceReg(&statu);
          //if(statu & 0x80){
            event_flag = FLAG_WaterMarkFull;
            LPM3_EXIT;
          //}
      }
        break;
        
        
     case P1IV_P1IFG2:                          //闪光灯，底座升级
#ifdef  IAP_ON 
       if(Flash_flag==0){
         TA1CCTL0  = CCIE;                      //开定时器中断
         bitCounter = 0;
         Flash_flag++;
         timer_20us_couter = 0;
         for(uint8_t i=0;i<8;i++)
           puse_arry[i]=0;
       }
       else{                                    //收集间隔时间进数组
         puse_arry[bitCounter++] = timer_20us_couter;   
         timer_20us_couter = 0;
         if(bitCounter==12){                    //收满了
          TA1CCTL0 &= ~CCIE;
          timer_20us_couter = 0;
          GPIO_disableInterrupt(GPIO_PORT_P1,    //关外部中断
                              FLASH_LIGHT_PIN);
           for(uint8_t i=0;i<10;i++){
             if((puse_arry[i] >= 190)&&(puse_arry[i] <= 210)){
              if((puse_arry[i+1] >= 32)&&(puse_arry[i+1] <= 40)&&
                 (puse_arry[i+2] >= 32)&&(puse_arry[i+2] <= 40)){
                 //Flash_flag = 7;
                 if(AD_value > LOW_VOLTAGE_TO_IAP){
                    TimeBackup();               //备份时间
                    dataReshape();                
                    WriteCurrentDataToEEPROM(); //数据保存。
                    /*临时*/
                    deleteRecord();             //擦除数据
                    erase_Seg(0x8201);          //擦除标志位
                    __delay_cycles(250000);
                    WDTCTL = WDTPW + 0x1100;    //重启
                 }
               }
             }
           }
           LPM3_EXIT;
         }
       }
#endif
          break;
     case P1IV_P1IFG3:                          //屏幕光通讯核心函数
        //上升沿触发。
       if((timer_1ms_couter>=120)&&(timer_1ms_couter<=320)){
           ScreenHeaderFlag = MY_YES;
           timer_1ms_TimeUp = 0;
           bitCounter = 0;
       }
       if((bitCounter<32)&&(ScreenHeaderFlag == MY_YES)){
         timer_1ms_TimeUp = 0;
         if((timer_1ms_couter>=45)&&(timer_1ms_couter<80)){
             screenByte[bitCounter/8] <<= 1;
             screenByte[bitCounter/8] += 1;
             bitCounter++;
         
         }
         else if((timer_1ms_couter>=80)&&(timer_1ms_couter<120)){
             screenByte[bitCounter/8]<<=1;
             bitCounter++;
         }
       }
       if(bitCounter==32)//凑满4个字节了。
       {  
         dataReceiveOff();
         switch((ScreenCMD)screenByte[0]){
         case SC_TIME:                          //校准时间
              if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
                dataReshape();
                WriteCurrentDataToEEPROM();
              }
              else{//激活
                wrt_flash_byte(POWER_ON_ADDR, POWER_VERF);
              }
              cal_time(&screenByte[0]);
              TimeBackup();
              //ledShow();
              init_CurrentData(DAY_MODE);
              creatMotionFlag();
              ActiveWorkMode();
             break;
         case SC_TAGET:                         //设定目标值
             if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
               setTarget();
               //ledShow();
             }else{
               bitCounter = 0;
             }
             break;
           case SC_ERASE:                       //擦除数据
             if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
               deleteRecord();
               //ledShow();
             }
             else{
               bitCounter = 0;
             }
             break;
           case 0xF0:                           //测试用。
              cal_time(&screenByte[0]);
              //ledShow();
             break;
           //case 0xAA:                         //设置参数
             //setArgument(&screenByte[0]);
             //震动检测
             //待机模式
             //参数调整
             //break;
           default:
             bitCounter = 0;
             break;
         }
       }
      timer_1ms_couter = 0;
      break;
     case P1IV_P1IFG4:                          //无线充电中断
         event_flag = FLAG_WirlessCharge;
         LPM3_EXIT;
      break; 
    default  :break;
  }
}

#pragma vector=RTC_VECTOR
__interrupt void RTC_ISR (void)
{
    switch (__even_in_range(RTCIV,16)){
        case 0: break;                          //No interrupts
        
        case 2:                                 //RTCRDYIFG,秒中断
           sec_interrupt_handle();
           break;
           
        case 4:                                 //分/时/零点/正午   
           min_interrupt_handle();
         break;
         
        case 6:                                 //闹钟中断
           alm_interrupt_handle();
         break;
         
        case 8:  break;                         //RT0PSIFG
        case 10: break;                         //RT1PSIFG
        default: break;
    }
}