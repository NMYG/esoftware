/* Includes ------------------------------------------------------------------*/
#include <msp430.h>
#include "ActRainbow.h"

#include "m24c64.h"


uint8_t M24C64_ReadByte(uint16_t ReadAddr, uint8_t* Data) {
  
  //开电源
  P2OUT |= 0x01; 
  delay_ms(2);
  
  UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
  UCB1CTL1 |= UCTR;                 // 写模式
  UCB1CTL1 |= UCTXSTT;              // 发送启动位和写控制字节

    
  UCB1TXBUF = ReadAddr>>8;            // 发送字节地址
  while(!(UCB1IFG & UCTXIFG))
  {
    if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
    {
      return 0;
    }
  }  
  // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
  
  UCB1TXBUF = ReadAddr;              // 发送字节地址
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1
  
  UCB1CTL1 &= ~UCTR;                // 读模式
  UCB1CTL1 |= UCTXSTT;              // 发送启动位和读控制字节

  while(UCB1CTL1 & UCTXSTT);        // 等待UCTXSTT=0
  // 若无应答 UCNACKIFG = 1
  UCB1CTL1 |= UCTXSTP;              // 先发送停止位

  while(!(UCB1IFG & UCRXIFG));      // 读取字节内容
  *Data = UCB1RXBUF;         // 读取BUF寄存器在发送停止位之后

  while( UCB1CTL1 & UCTXSTP );

  
  delay_ms(1);
  //关电源
  P2OUT &= 0xfe; 
  
  return 1; 

}

uint8_t M24C64_ReadByte_PowerON(uint16_t ReadAddr, uint8_t* Data) {
  
  UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
  UCB1CTL1 |= UCTR;                 // 写模式
  UCB1CTL1 |= UCTXSTT;              // 发送启动位和写控制字节

    
  UCB1TXBUF = ReadAddr>>8;            // 发送字节地址
  while(!(UCB1IFG & UCTXIFG))
  {
    if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
    {
      return 0;
    }
  }  
  // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
  
  UCB1TXBUF = ReadAddr;              // 发送字节地址
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1
  
//UCB1TXBUF = ReadAddr;             // 发送字节地址，必须要先填充TXBUF
// 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
                      

    UCB1CTL1 &= ~UCTR;                // 读模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和读控制字节

    while(UCB1CTL1 & UCTXSTT);        // 等待UCTXSTT=0
    // 若无应答 UCNACKIFG = 1
    UCB1CTL1 |= UCTXSTP;              // 先发送停止位

    while(!(UCB1IFG & UCRXIFG));      // 读取字节内容
    *Data = UCB1RXBUF;         // 读取BUF寄存器在发送停止位之后

    while( UCB1CTL1 & UCTXSTP );

    return 1; 

}



uint8_t M24C64_WriteByte(uint16_t WriteAddr, uint8_t Data) {
  
  //开电源
  P2OUT |= 0x01; 
  delay_ms(2);
  
  UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
  while( UCB1CTL1 & UCTXSTP );
  UCB1CTL1 |= UCTR;                 // 写模式
  UCB1CTL1 |= UCTXSTT;              // 发送启动位

  UCB1TXBUF = WriteAddr>>8;            // 发送字节地址
    while(!(UCB1IFG & UCTXIFG))
  {
    if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
    {
      return 0;
    }
  }  
  // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
  
  UCB1TXBUF = WriteAddr;            // 发送字节地址低8位
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1

  UCB1TXBUF = Data;           // 发送字节内容
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1

  UCB1CTL1 |= UCTXSTP;
  while(UCB1CTL1 & UCTXSTP);        // 等待发送完成
  
  
    
  delay_ms(6);
  //关电源
  P2OUT &= 0xfe; 
  return 1;
}
uint8_t M24C64_WriteByte_PowerON(uint16_t WriteAddr, uint8_t Data) {
  
  
  UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
  while( UCB1CTL1 & UCTXSTP );
  UCB1CTL1 |= UCTR;                 // 写模式
  UCB1CTL1 |= UCTXSTT;              // 发送启动位

  UCB1TXBUF = WriteAddr>>8;            // 发送字节地址
    while(!(UCB1IFG & UCTXIFG))
  {
    if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
    {
      return 0;
    }
  }  
  // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
  
  UCB1TXBUF = WriteAddr;            // 发送字节地址低8位
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1

  UCB1TXBUF = Data;           // 发送字节内容
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1

  UCB1CTL1 |= UCTXSTP;
  while(UCB1CTL1 & UCTXSTP);        // 等待发送完成
  
  return 1;
}

uint8_t M24C64_ReadPage(uint16_t ReadAddr, uint8_t* Data) {
 
    uint8_t i = 0;
    
    //开电源
    P2OUT |= 0x01; 
    delay_ms(2);
    
    UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
    UCB1CTL1 |= UCTR;                 // 写模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和写控制字节

    UCB1TXBUF = ReadAddr >> 8;             // 发送字节地址，必须要先填充TXBUF
    // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
    while(!(UCB1IFG & UCTXIFG))
    {
      if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
      {
        return 0;
      }
    }
    
    UCB1TXBUF = ReadAddr;            // 发送字节地址低8位
    while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1

    UCB1CTL1 &= ~UCTR;                // 读模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和读控制字节

    while(UCB1CTL1 & UCTXSTT);        // 等待UCTXSTT=0
    // 若无应答 UCNACKIFG = 1

    for(i=0;i<PAGE_BYTES;i++){        
      if((PAGE_BYTES-1) == i)
          UCB1CTL1 |= UCTXSTP;           // 读到最后一个地址，先发送停止位
      while(!(UCB1IFG & UCRXIFG));      // 读取字节内容
      *Data = UCB1RXBUF;                // 读取BUF寄存器在发送停止位之后
      Data++;
    }    
    while( UCB1CTL1 & UCTXSTP );
    
    delay_ms(1);
    //关电源
    P2OUT &= 0xfe; 

    return 1; 

}

uint8_t M24C64_ReadPageWithLen(uint16_t ReadAddr, uint8_t* Data,uint8_t len) {
 
    uint8_t i = 0;
    
    
  if(0 == len) return 0;
    //开电源
    P2OUT |= 0x01; 
    delay_ms(2);
    
    UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
    UCB1CTL1 |= UCTR;                 // 写模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和写控制字节

    UCB1TXBUF = ReadAddr >> 8;             // 发送字节地址，必须要先填充TXBUF
    // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
    while(!(UCB1IFG & UCTXIFG))
    {
      if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
      {
        return 0;
      }
    }
    
    UCB1TXBUF = ReadAddr;            // 发送字节地址低8位
    while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1

    UCB1CTL1 &= ~UCTR;                // 读模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和读控制字节

    while(UCB1CTL1 & UCTXSTT);        // 等待UCTXSTT=0
    // 若无应答 UCNACKIFG = 1

    for(i=0;i<len;i++){        
      if((len-1) == i)
          UCB1CTL1 |= UCTXSTP;           // 读到最后一个地址，先发送停止位
      while(!(UCB1IFG & UCRXIFG));      // 读取字节内容
      *Data = UCB1RXBUF;                // 读取BUF寄存器在发送停止位之后
      Data++;
    }    
    while( UCB1CTL1 & UCTXSTP );
    
    
    delay_ms(6);
    //关电源
    P2OUT &= 0xfe; 

    return 1; 

}

uint8_t M24C64_WritePage(uint16_t WriteAddr, uint8_t* Data) {
  
  uint8_t i;
  
  //开电源
  P2OUT |= 0x01; 
  delay_ms(2);

  UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
  while( UCB1CTL1 & UCTXSTP );
  UCB1CTL1 |= UCTR;                 // 写模式
  UCB1CTL1 |= UCTXSTT;              // 发送启动位

  UCB1TXBUF = WriteAddr >> 8;            // 发送字节地址
  // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
  while(!(UCB1IFG & UCTXIFG))
  {
    if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
    {
      return 0;
    }
  }
  
  UCB1TXBUF = WriteAddr;            // 发送字节地址低8位
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1
  
  for(i=0;i<PAGE_BYTES;i++){
      UCB1TXBUF = *Data;           // 发送字节内容
      while(!(UCB1IFG & UCTXIFG));// 等待UCTXIFG=1
      Data++;
  }

  UCB1CTL1 |= UCTXSTP;
  while(UCB1CTL1 & UCTXSTP);        // 等待发送完成
  
  
  delay_ms(6);
  //关电源
  P2OUT &= 0xfe; 
  
  return 1;
}

uint8_t M24C64_WritePageWithLen(uint16_t WriteAddr,
                             uint8_t* Data,uint8_t len) {
  
  uint8_t i;
  
  if(0 == len) return 0;
  
  //开电源
  P2OUT |= 0x01;
  delay_ms(2);

  UCB1I2CSA = EEPROM_ADDRESS;        // EEPROM_ADDRESS
  while( UCB1CTL1 & UCTXSTP );
  UCB1CTL1 |= UCTR;                 // 写模式
  UCB1CTL1 |= UCTXSTT;              // 发送启动位

  UCB1TXBUF = WriteAddr >> 8;            // 发送字节地址
  // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
  while(!(UCB1IFG & UCTXIFG))
  {
    if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
    {
      return 0;
    }
  }
  
  UCB1TXBUF = WriteAddr;            // 发送字节地址低8位
  while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1
  
  for(i=0;i<len;i++){
      UCB1TXBUF = *Data;           // 发送字节内容
      while(!(UCB1IFG & UCTXIFG));// 等待UCTXIFG=1
      Data++;
  }

  UCB1CTL1 |= UCTXSTP;
  while(UCB1CTL1 & UCTXSTP);        // 等待发送完成
  
  delay_ms(6);
  //关电源
  P2OUT &= 0xfe; 
  
  return 1;
}

/*
uint8_t M24C64_WriteOverPageWithLen(uint16_t WriteAddr,
                             uint8_t* Data,uint8_t len) {
  
  uint8_t part1=0,part2=0;
  
  if(0 == len) return 0;
  if((WriteAddr%128)>0)
    part1 = 128-(WriteAddr%128);
  else
    part1=128;
  
  if(len<=part1){
    part1 = len;
    part2 = 0;
  }
  else{
    part2 = len-part1;
  }
  M24C64_WritePageWithLen(WriteAddr,Data,part1);
  M24C64_WritePageWithLen(WriteAddr+part1,(Data+part1),part2);
  
  return 1;
}
*/
