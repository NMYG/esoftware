#include "ActRainbow.h"
#include "core.h"
#include "Encode.h"
#include "FLASH_EEPROM.H"
#include <stdio.h>
#include "led.h"
#include "string.h"
#include "uart1.h"
#include "lis3dh_driver.h"
//#include <stdlib.h>


#define TRIGGER()     GPIO_toggleOutputOnPin(GPIO_PORT_P4,BIT5) 

#define MAX_LEN_DATA (8*120)
#define MIN_LEN_DATA (8*60)


#define MAX_LEN_MOTION (8*120)
#define MIN_LEN_MOTION (8*60)

uint8_t g_fifo_watrMark_lever = 25;
uint16_t G_montion_Value = 0;
uint8_t g_old_date = 1;

float sc_montion = 0;//每次传感器采样周期的运动量


bool g_wait_turn = false;

//==================================================
BlockInfo_Def           BlockInfo;//当前的EEPROM的使用情况。
CurrentData_def         CurrentData;//保存当前的数据在RAM中，以减少读写ROM的次数

Z_FLAG Z_axis_statu = flag_ZT;

Z_AXIS_VAULE  z_axis_value;//

//=============运动量输出指针=====================================
uint16_t logAddress                = (uint16_t)(LOG_PAGE_ADDR+7);
volatile uint16_t motionAddress   = MOTION_PAGE_ADDR;
uint16_t  motionCutAddress         = 0;

//==============运动量计算=========
int16_t ig_x_att= 0;
int16_t ig_y_att= 0;
int16_t ig_z_att= 0;


uint8_t g_sc_montion[20]={0};
uint8_t g_sc_index = 20;
//==============计步算法相关==============
//#define COMPARE_VALUE 2184
//#define SAMPLE_QUANTITY 25

 /*  
  //50HZ 
#define SAMPLE_QUANTITY 50
#define TIME_WINDOW_MIN 10
#define TIME_WINDOW_MAX 35
#define ANGL_APP  2.0f
#define G_BUFFER_QUANTITY  5
#define ANGL_QUANTITY      7
*/
/*
  /////20HZ
#define SAMPLE_QUANTITY 20
#define TIME_WINDOW_MIN 3
#define TIME_WINDOW_MAX 10
#define ANGL_APP  2.0f
#define G_BUFFER_QUANTITY  5
#define ANGL_QUANTITY      7
*/


//10HZ
#define SAMPLE_QUANTITY         25
#define SAMPLE_QUANTITY2        25
#define TIME_WINDOW_MIN         6//3
#define TIME_WINDOW_MAX         15//10
#define ANGL_APP                2.0f//2.0f
#define G_BUFFER_QUANTITY       5//3
#define ANGL_QUANTITY           7

//==========这段全局变量太多了，找时间优化一下=========

uint8_t   STEPS = 0;
short RealSample[3];
AxesRaw_t buff_raw;
unsigned int delay_step1 = 0;
unsigned int tempStep  = 0;
unsigned int delay_step2 = 0;
//short ax,ay,az;   
//float sqrt_sample;
//float angl_sample;
//float g_x,g_y,g_z;

float g_x_old[G_BUFFER_QUANTITY],g_y_old[G_BUFFER_QUANTITY],g_z_old[G_BUFFER_QUANTITY];
float angl_sample_old[ANGL_QUANTITY];
float angl_sample_sum = 0;
//float angl_sample_sum1;
unsigned char sampling_counter = SAMPLE_QUANTITY2;//0;

float		angl_sample_sum_old;
float		angl_app;
float		angl_threshold;
float		max_angl=-1;
float		min_angl =10000;
unsigned char	angl_TimerCnt=0;
unsigned char angl_TimerCnt_old[3]={0};
unsigned char	angl_fgPossitive=0;
unsigned char	angl_fgNegative=0;

float		max_g_x = -100.0f;
float		max_g_y = -100.0f;
float		max_g_z = -100.0f;
float		min_g_x = 100.0f;
float		min_g_y = 100.0f;
float		min_g_z = 100.0f;
float          g_x_app;
float          g_y_app;
float          g_z_app;

unsigned char angl_buffer[30];
#ifdef UART_ON
char temp_buffer[50];
#endif

uint32_t   ALLSTEPS = 0;
//uint8_t sec_step[10]={0};
//uint8_t step_tick=0;

unsigned int   TagetStep = 8000;//目标值

uint8_t lightOnCycle = 1;
uint16_t montion_lever_1m[TIME_INTO_SLEEP] = {0};

uint32_t G_Value =0;
//存储3轴加速度值的数据结构。
AxesRaw_t AxesBuffer[30];

void dataReceiveOpen(void);
/*退出待机模式*/
void OutOfStandbyMode(){
  
   if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
    reakBlockInfoFromEEPROM();
    
    init_CurrentData(DAY_MODE);
    creatMotionFlag();
   }
    ActiveWorkMode();
   
   montion_lever_1m[0]=0;
   montion_lever_1m[1]=199;
}

/*设置为未激活模式*/
void unActiveMode(){
    
    //RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);
  
    GPIO_enableInterrupt(GPIO_PORT_P1,
                          //LIS3DH_INT2_PIN+
                          LIS3DH_INT1_PIN
                          //FLASH_LIGHT_PIN
                          //DATA_LIGHT_PIN
                          );
    
    GPIO_disableInterrupt(GPIO_PORT_P1,
                           LIS3DH_INT2_PIN+
                           //LIS3DH_INT1_PIN+
                           FLASH_LIGHT_PIN+
                           DATA_LIGHT_PIN);
    //清除中断标志位。
    GPIO_clearInterruptFlag(GPIO_PORT_P1,
                            LIS3DH_INT2_PIN+
                            LIS3DH_INT1_PIN+
                            FLASH_LIGHT_PIN+
                            DATA_LIGHT_PIN);
    //关闭EEPROM
    GPIO_setOutputLowOnPin(V_EEPROM_PROT,
                           V_EEPROM_PIN); 
    //屏幕光通讯Photodiode电源关闭
    GPIO_setOutputLowOnPin(V_DATA_PHOTODIODE_PORT,
                           V_DATA_PHOTODIODE_PIN);
    
    LIS3DH_MutilReadFIFO( LIS3DH_FIFO_START, AxesBuffer, g_fifo_watrMark_lever);
  
    i2c_config();
    LIS3DH_WriteReg(LIS3DH_CTRL_REG5,0x80);//reboot
    delay_ms(100);
    //取消FIFO中断，设置阈值中断
    LIS3DH_SetMode(LIS3DH_NORMAL);
    
    LIS3DH_SetODR(LIS3DH_ODR_25Hz);
    
    LIS3DH_SetFullScale(LIS3DH_FULLSCALE_4);
    LIS3DH_SetAxis(LIS3DH_X_ENABLE | LIS3DH_Y_ENABLE | LIS3DH_Z_ENABLE);
    
    //500mg
    LIS3DH_SetInt1Threshold(0x30);//1G  40
    //Duration =0
    LIS3DH_SetInt1Duration(0);
    
    LIS3DH_SetIntMode(LIS3DH_INT_MODE_6D_MOVEMENT);
    //enable XH and YH and ZH interupt generation  
    LIS3DH_SetIntConfiguration(LIS3DH_INT1_ZHIE_ENABLE | LIS3DH_INT1_ZLIE_ENABLE |
			       LIS3DH_INT1_YHIE_ENABLE | LIS3DH_INT1_YLIE_ENABLE |
			       LIS3DH_INT1_XHIE_ENABLE | LIS3DH_INT1_XLIE_ENABLE ); 
    
    //interrupt drivern to INT1 Pad
    LIS3DH_SetInt1Pin(  LIS3DH_CLICK_ON_PIN_INT1_DISABLE 
                      | LIS3DH_I1_INT1_ON_PIN_INT1_ENABLE  
                      | LIS3DH_I1_INT2_ON_PIN_INT1_DISABLE 
                      | LIS3DH_I1_DRDY1_ON_INT1_DISABLE 
                      | LIS3DH_I1_DRDY2_ON_INT1_DISABLE 
                      | LIS3DH_WTM_ON_INT1_DISABLE 
                      | LIS3DH_INT1_OVERRUN_DISABLE);
}
/*进入待机模式*/
void intoStandbyMode(){
    //建立一个待机模式标志位
    init_CurrentData(Standby_MODE);
    WriteCurrentDataToEEPROM();
    
    //使能分钟中断。
    RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);
    
    //失能秒中断。
    RTC_A_disableInterrupt(RTC_A_BASE,RTCRDYIE);
    
    unActiveMode();
}
/*开关中断--为彩光通信服务*/
void Switch_interrupter(MY_FLAG flag){
  switch(flag){
    case MY_NO:
      //关闭分钟中断。
      RTC_A_disableInterrupt(RTC_A_BASE,RTCTEVIE);
      //外部中断失能
      GPIO_disableInterrupt(GPIO_PORT_P1,
                              //LIS3DH_INT2_PIN+
                              LIS3DH_INT1_PIN+
                              FLASH_LIGHT_PIN+
                              DATA_LIGHT_PIN);
      //充电失能
      GPIO_disableInterrupt(V_CHG_PORT,
                           V_CHG_PIN);
      
      break;
  case MY_YES:
      
      //使能分钟中断。
      RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);
      //外部中断使能
      GPIO_enableInterrupt(GPIO_PORT_P1,
                              //LIS3DH_INT2_PIN+
                              LIS3DH_INT1_PIN
                              //FLASH_LIGHT_PIN
                              //DATA_LIGHT_PIN
                              );
       //中断使能
      GPIO_enableInterrupt(V_CHG_PORT,
                           V_CHG_PIN);
      
      break;
  }
}
/*设置为睡眠监测模式*/
resualt_t SleepMonitorMode(){
  g_fifo_watrMark_lever = 30;
  LIS3DH_Init(LIS3DH_ODR_10Hz);
  //设置每分钟一次中断
  RTC_A_setCalendarEvent(RTC_A_BASE,
              RTC_A_CALENDAREVENT_MINUTECHANGE);
  //使能分钟中断。
  RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);
  
  //外部中断使能
  GPIO_enableInterrupt(GPIO_PORT_P1,
                          //LIS3DH_INT2_PIN+
                          LIS3DH_INT1_PIN
                          //FLASH_LIGHT_PIN
                          //DATA_LIGHT_PIN
                          );
  //清除中断标志位。
  GPIO_clearInterruptFlag(GPIO_PORT_P1,
                          LIS3DH_INT2_PIN+
                          LIS3DH_INT1_PIN+
                          FLASH_LIGHT_PIN+
                          DATA_LIGHT_PIN);
  //激活FIFO
  LIS3DH_MutilReadFIFO(LIS3DH_FIFO_START,
                      AxesBuffer,
                      g_fifo_watrMark_lever);

  return SUCCESS;
}
/*设置为白天工作模式*/
resualt_t ActiveWorkMode(){
  g_fifo_watrMark_lever = 25;
  LIS3DH_Init(LIS3DH_ODR_25Hz);
  //设置每分钟一次中断
  RTC_A_setCalendarEvent(RTC_A_BASE,
              RTC_A_CALENDAREVENT_MINUTECHANGE);
  //使能分钟中断。
  RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);
  
  //外部中断使能
  GPIO_enableInterrupt(GPIO_PORT_P1,
                          //LIS3DH_INT2_PIN+
                          LIS3DH_INT1_PIN
                          //FLASH_LIGHT_PIN
                          //DATA_LIGHT_PIN
                          );
  //清除中断标志位。
  GPIO_clearInterruptFlag(GPIO_PORT_P1,
                          LIS3DH_INT2_PIN+
                          LIS3DH_INT1_PIN+
                          FLASH_LIGHT_PIN+
                          DATA_LIGHT_PIN);
  //激活FIFO
  LIS3DH_MutilReadFIFO(LIS3DH_FIFO_START,
                      AxesBuffer,
                      g_fifo_watrMark_lever);

  return SUCCESS;
}

//进入无线充电模式
void enterChargingMode()
{
    LIS3DH_SetMode(LIS3DH_POWER_DOWN);

    //外部中断关闭
    GPIO_disableInterrupt(GPIO_PORT_P1,
                           LIS3DH_INT2_PIN+
                           LIS3DH_INT1_PIN+
                           FLASH_LIGHT_PIN+
                           DATA_LIGHT_PIN);
    //关闭EEPROM
    GPIO_setOutputLowOnPin(V_EEPROM_PROT,
                              V_EEPROM_PIN); 
    //屏幕光通讯Photodiode电源关闭
    GPIO_setOutputLowOnPin(V_DATA_PHOTODIODE_PORT,
                           V_DATA_PHOTODIODE_PIN);
    
    //使能秒中断。
    RTC_A_enableInterrupt(RTC_A_BASE,RTCRDYIE);
    
  if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
    //数据整形
    dataReshape();
    //把现在的数据保存好。
    WriteCurrentDataToEEPROM();
  }
}

/*FIFO中断来临处理*/
resualt_t ActRainbow_FIFORoutine(){
  
    __disable_interrupt();
    LIS3DH_MutilReadFIFO( LIS3DH_FIFO_START, AxesBuffer, g_fifo_watrMark_lever);
    __enable_interrupt();
    
    g_sc_index++;
    if(g_sc_index>=20) g_sc_index = 0;
    attitude_Montion_Cal();
    g_sc_montion[g_sc_index] = (uint8_t)sc_montion;
    if(g_pwm_show){ return SUCCESS;}
    if(z_axis_value.Value_zd>=22){
#ifdef  IAP_ON      
      /*===========判断是否有IAP信号============*/
      //先关掉不要的中断
      RTC_A_disableInterrupt(RTC_A_BASE,RTCTEVIE);//分钟中断
      //外部中断失能
      GPIO_disableInterrupt(GPIO_PORT_P1,//ACC中断
                              LIS3DH_INT1_PIN);
      //开始判断
      Flash_flag = 0;
      timer_20us_couter = 0;
      GPIO_enableInterrupt(GPIO_PORT_P1,//开外部中断
                    FLASH_LIGHT_PIN);
      
      TA1CCR0 = FLASH_LIGHT_COMPARE_VALUE-1;      //20us                   
      TA1CTL = TASSEL_2 + MC_1 + TACLR ;        // SMCLK, upmode, clear TAR  
      TA1CCTL0 = CCIE;//开定时器中断
      /*春暖花开，万物滋生，这是生育，哦不，是升级的好时节*/
      LPM3;
      /*春天过了，什么事都没有发生*/
      //打开中断
      TA1CTL = TASSEL_2 + MC_0 + TACLR ;        // SMCLK, STOP, clear TAR 
      RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);//分钟中断
      GPIO_enableInterrupt(GPIO_PORT_P1,//ACC中断
                            LIS3DH_INT1_PIN);  //27
      
      GPIO_disableInterrupt(GPIO_PORT_P1,//关外部中断
                              FLASH_LIGHT_PIN);
      
      /*================结束===================*/
#endif
      if(Z_axis_statu != flag_ZD){
        Z_axis_statu = flag_ZD;
#ifdef DEBUG_LIGHT   
        clock_light_color_AllOff();
#endif        
        dataReceiveOpen();
        LPM3;
        dataReceiveOff();
        if(bitCounter == 32){
            //等待翻转
            g_wait_turn = true;
            LIS3DH_MutilReadFIFO(LIS3DH_FIFO_START,
                                AxesBuffer,
                                g_fifo_watrMark_lever);
            bitCounter = 0;
        }
        else{
           do_lightJob();
        }
      }
    }
    else if((z_axis_value.Value_zu>=24)&&
            (POWER_VERF == (*((uint8_t *)POWER_ON_ADDR)))){//27
      if(Z_axis_statu != flag_ZU){
         Z_axis_statu = flag_ZU;
         showProgress();
      }
    }

    else if((sc_montion>=1.1f)&&((DAY_MODE == CurrentData.Mode))&&
            (POWER_VERF == (*((uint8_t *)POWER_ON_ADDR)))){
        StepCountHandler();
        return SUCCESS;
    }
    delay_step2++;
    if(2 <= delay_step2){//连续的3周期之内没有计步数据就把delay_step1清空
      delay_step1 = 0;
      delay_step2 = 0;
      tempStep = 0;
    }
    return SUCCESS;
}

/*判断姿态，计算运动量*/
void attitude_Montion_Cal(){
  
float temp_sqrtX = 0;
float temp_sqrtY = 0;
float temp_sqrtZ = 0;
        
#ifdef  VIB_DET_ON   
    uint8_t start =0;
    uint8_t frq1 =0;
    uint8_t frq2 =0;
#endif
    uint16_t data_cnt;
    
    sc_montion=0;
    z_axis_value.Value_zt=0;
    z_axis_value.Value_zd=0;
    z_axis_value.Value_zu=0;
    z_axis_value.Value_zz=0;
    
//    float sqrt_sample_new =0 ;
    
    ig_x_att = (AxesBuffer[0].AXIS_X);
    ig_y_att = (AxesBuffer[0].AXIS_Y);
    ig_z_att = (AxesBuffer[0].AXIS_Z);
    
    for(data_cnt=0;data_cnt < g_fifo_watrMark_lever ;data_cnt++){
      
        //==============整型姿态判断===============//1000==0.12
        if((AxesBuffer[data_cnt].AXIS_X> -1000)&&
           (AxesBuffer[data_cnt].AXIS_X< 1000)&&
             (AxesBuffer[data_cnt].AXIS_Y> -1000)&&
               (AxesBuffer[data_cnt].AXIS_Y< 1000)&&
                (AxesBuffer[data_cnt].AXIS_Z> 7000)&&
                 (AxesBuffer[data_cnt].AXIS_Z< 9200)){//8192==1
          z_axis_value.Value_zd++;
        }
        else if((AxesBuffer[data_cnt].AXIS_X> -1000)&&
           (AxesBuffer[data_cnt].AXIS_X< 1000)&&
             (AxesBuffer[data_cnt].AXIS_Y> -1000)&&
               (AxesBuffer[data_cnt].AXIS_Y< 1000)&&
                (AxesBuffer[data_cnt].AXIS_Z> -9300)&&
                (AxesBuffer[data_cnt].AXIS_Z< -6800)){
          z_axis_value.Value_zu++;
        }else if((AxesBuffer[data_cnt].AXIS_Z> -1000)&&
                (AxesBuffer[data_cnt].AXIS_Z< 1000)){
          z_axis_value.Value_zz++;
        }
    }
    //===========================================
    //有了姿态就不需要计算运动量
    if(!((z_axis_value.Value_zd>=23)||(z_axis_value.Value_zu>=23)||
         (z_axis_value.Value_zz>=23))){
        for(data_cnt=1;data_cnt < g_fifo_watrMark_lever ;data_cnt++){
          if(AxesBuffer[data_cnt].AXIS_X > ig_x_att ){
            ig_x_att = AxesBuffer[data_cnt].AXIS_X - ig_x_att;}
          else{
            ig_x_att = ig_x_att - AxesBuffer[data_cnt].AXIS_X;}
          
          if(AxesBuffer[data_cnt].AXIS_Y > ig_y_att ){
            ig_y_att = AxesBuffer[data_cnt].AXIS_Y - ig_y_att;}
          else{
            ig_y_att = ig_y_att - AxesBuffer[data_cnt].AXIS_Y;}
          
          if(AxesBuffer[data_cnt].AXIS_Z > ig_z_att ){
            ig_z_att = AxesBuffer[data_cnt].AXIS_Z - ig_z_att;}
          else{
            ig_z_att = ig_z_att - AxesBuffer[data_cnt].AXIS_Z;}
          temp_sqrtX = (ig_x_att*4)/32766.0f;
          temp_sqrtY = (ig_y_att*4)/32766.0f;
          temp_sqrtZ = (ig_z_att*4)/32766.0f;
//            sc_montion += sqrt(
//                   (((ig_x_att*4)/32766.0f) * ((ig_x_att*4)/32766.0f))+
//                    (((ig_y_att*4)/32766.0f) * ((ig_y_att*4)/32766.0f))+
//                      (((ig_z_att*4)/32766.0f) * ((ig_z_att*4)/32766.0f)));
          sc_montion += sqrt((temp_sqrtX*temp_sqrtX)+
                             (temp_sqrtY*temp_sqrtY)+
                             (temp_sqrtZ*temp_sqrtZ));
          //缓存
          ig_x_att = (AxesBuffer[data_cnt].AXIS_X);
          ig_y_att = (AxesBuffer[data_cnt].AXIS_Y);
          ig_z_att = (AxesBuffer[data_cnt].AXIS_Z);
       }
    }
    
    if((Z_axis_statu != flag_ZT)&&(sc_montion>=4)){
        Z_axis_statu = flag_ZT;
    }
    if((sc_montion>=1)&&(g_wait_turn)){
      ledShow();
      //激活FIFO
      LIS3DH_MutilReadFIFO(LIS3DH_FIFO_START,
                          AxesBuffer,
                          g_fifo_watrMark_lever);
      g_wait_turn = false; 
    }
   //累计本分钟内的运动量
   montion_lever_1m[0] += (uint16_t)(sc_montion);
}


/*屏幕光通信开启*/
void dataReceiveOpen(void)
{
    
    //关闭分钟中断。
    RTC_A_disableInterrupt(RTC_A_BASE,RTCTEVIE);
    
    //外部中断失能
    GPIO_disableInterrupt(GPIO_PORT_P1,
                              //LIS3DH_INT2_PIN+
                              LIS3DH_INT1_PIN+
                              FLASH_LIGHT_PIN);
      
    GPIO_enableInterrupt(GPIO_PORT_P1,
                          //LIS3DH_INT2_PIN+
                          DATA_LIGHT_PIN);
    //V_D电源开
    GPIO_setOutputHighOnPin(V_DATA_PHOTODIODE_PORT,
                            V_DATA_PHOTODIODE_PIN);
    ScreenHeaderFlag =MY_NO;
    
    //定时器A用于计算黑白光间隔。
    timer_1ms_couter = 0;timer_1ms_TimeUp = 0;
    TA0CCTL0 = CCIE;
    //清空变量
    for(uint8_t i=0;i<4;i++){
      screenByte[i] = 0;
    }
    bitCounter=0;
}
/*屏幕光通信关闭*/
void dataReceiveOff(void)
{
  
    //clock_light_off(CLOCK_GREEN,1);
    GPIO_setOutputLowOnPin(V_DATA_PHOTODIODE_PORT,
                            V_DATA_PHOTODIODE_PIN);//电源关
    //打开外部中断
    GPIO_enableInterrupt(GPIO_PORT_P1,
                            //LIS3DH_INT2_PIN+
                            LIS3DH_INT1_PIN
                            //FLASH_LIGHT_PIN
                            );
    
    GPIO_disableInterrupt(GPIO_PORT_P1,
                              //LIS3DH_INT2_PIN+
                              DATA_LIGHT_PIN);
    //关闭定时器
    timer_1ms_couter = 0;
    TA0CCTL0 &= ~CCIE;
    //使能分钟中断。
    RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);
}

/*求均方差*/
float XYStandard_Deviation(MY_AXIS axis){
   
    float AvrValue;
    float tempValue1;
    uint8_t data_cnt = 0;
    int16_t target_axis[25];
    //选择轴向
    switch(axis){
      case AXIS_X:
        for(data_cnt=0;data_cnt<g_fifo_watrMark_lever;data_cnt++)
             target_axis[data_cnt] = AxesBuffer[data_cnt].AXIS_X;
        break;
        
      case AXIS_Y:
        for(data_cnt=0;data_cnt<g_fifo_watrMark_lever;data_cnt++)
             target_axis[data_cnt] = AxesBuffer[data_cnt].AXIS_Y;
        break;
        
      case AXIS_Z:
        for(data_cnt=0;data_cnt<g_fifo_watrMark_lever;data_cnt++)
             target_axis[data_cnt] = AxesBuffer[data_cnt].AXIS_Z;
        break;
    }
  //求均方差
   for(data_cnt=0;data_cnt < g_fifo_watrMark_lever ;data_cnt++){
      tempValue1 += target_axis[data_cnt];
   }
     AvrValue=tempValue1/g_fifo_watrMark_lever;//得到平均值
   
   tempValue1 = 0;
   for(data_cnt=0;data_cnt < g_fifo_watrMark_lever ;data_cnt++){
     tempValue1 += ((target_axis[data_cnt] - AvrValue) *
                 (target_axis[data_cnt] - AvrValue));
   }
   tempValue1=tempValue1/g_fifo_watrMark_lever;
 
   tempValue1 = sqrt(tempValue1);//得到X标准差。
   
   return tempValue1;
}


/*计步运算*/
void StepCountHandler(void)
{
  
  uint16_t arv_montion = 0;
  uint8_t data_cnt = 0;
  float sqrt_sample;
  float g_x,g_y,g_z;
  float angl_sample_sum1;
  uint8_t i;
  int16_t axis_x;
  int16_t axis_y;
  int16_t axis_z;
  
//  u8_t   STEPS = 0;
  STEPS = 0;
  for(data_cnt=0;data_cnt < SAMPLE_QUANTITY ;data_cnt++)
  {//待处理的数据为FIFO_WATERMAKR_LEVER个XYZ轴数据。
    
    //if(data_cnt2 % 3 == 2) data_cnt2++;
    axis_x = AxesBuffer[data_cnt].AXIS_X;
    axis_y = AxesBuffer[data_cnt].AXIS_Y;
    axis_z = AxesBuffer[data_cnt].AXIS_Z;
    
    for(i=0;i<G_BUFFER_QUANTITY-1;i++)
    {
      g_x_old[G_BUFFER_QUANTITY-1-i] = g_x_old[G_BUFFER_QUANTITY-i-2];
      g_y_old[G_BUFFER_QUANTITY-1-i] = g_y_old[G_BUFFER_QUANTITY-i-2];
      g_z_old[G_BUFFER_QUANTITY-1-i] = g_z_old[G_BUFFER_QUANTITY-i-2];
    }
    g_x_old[0] = axis_x/32767.0f*G_SCALE;
    g_y_old[0] = axis_y/32767.0f*G_SCALE;
    g_z_old[0] = axis_z/32767.0f*G_SCALE;
    
    g_x=0;
    g_y=0;
    g_z=0;
      
    for(i=0;i<G_BUFFER_QUANTITY;i++)
    {
      g_x += g_x_old[i]/G_BUFFER_QUANTITY;
      g_y += g_y_old[i]/G_BUFFER_QUANTITY;
      g_z += g_z_old[i]/G_BUFFER_QUANTITY;
    }
      sqrt_sample = sqrt(g_x*g_x+g_y*g_y+g_z*g_z);
      
    for(i=0;i<ANGL_QUANTITY;i++)
    {
      angl_sample_old[ANGL_QUANTITY-1-i] = angl_sample_old[ANGL_QUANTITY-i-2];
    }
    
    angl_sample_old[0] =(acos(g_x/sqrt_sample)*acos(g_y/sqrt_sample)+ 
                         acos(g_z/sqrt_sample)*acos(g_y/sqrt_sample)+
                           acos(g_x/sqrt_sample)*acos(g_z/sqrt_sample));
  
    angl_sample_sum_old  =  angl_sample_sum;
    angl_sample_sum1 =0.0f;
    for(i=0;i<ANGL_QUANTITY;i++)
    {
      angl_sample_sum1 += angl_sample_old[i];
    }
    angl_sample_sum =  angl_sample_sum1*angl_sample_sum1;

    if(g_x > max_g_x)     {max_g_x = g_x;}
    if(g_x < min_g_x)     {min_g_x = g_x;}
       
    if(g_y > max_g_y)     {max_g_y = g_y;}
    if(g_y < min_g_y)     {min_g_y = g_y;}
       
    if(g_z > max_g_z)     {max_g_z  = g_z;}
    if(g_z < min_g_z)     {min_g_z  = g_z;}
       
    if(angl_sample_sum > max_angl)     {max_angl = angl_sample_sum;}
    if(angl_sample_sum < min_angl)     {min_angl = angl_sample_sum;}
           
    if(sampling_counter++ > SAMPLE_QUANTITY2)
    {
      sampling_counter = 0;
      angl_app=max_angl-min_angl;        
      g_x_app=max_g_x-min_g_x;
      g_y_app=max_g_y-min_g_y;
      g_z_app=max_g_z-min_g_z;
      angl_threshold =min_angl+0.5*angl_app;
      max_angl =  -1.0f ;
      min_angl =  10000.0f;
      max_g_x = -100.0f;
      max_g_y = -100.0f;
      max_g_z = -100.0f;
      min_g_x = 100.0f;
      min_g_y = 100.0f;
      min_g_z = 100.0f;
    }
    if (( angl_sample_sum - angl_sample_sum_old > 0.8 )&&
        ((angl_fgPossitive == 0) && (angl_sample_sum_old < angl_threshold)))
    {     
      angl_TimerCnt = 0;
      angl_fgPossitive = 1;
      angl_fgNegative= 0;
    }
    if (( angl_sample_sum_old - angl_sample_sum> 0.8 )&&
        ((angl_fgNegative == 0) && (angl_sample_sum_old > angl_threshold)))
    {     
      angl_TimerCnt =  0;
      angl_fgPossitive = 0;
      angl_fgNegative= 1;
    }	
    if (angl_TimerCnt++ >= SAMPLE_QUANTITY2)
    {    
      angl_TimerCnt =  0;
      angl_fgPossitive = 0;
      angl_fgNegative= 0; 
    }
    if (angl_app >= ANGL_APP)
    {
      if((angl_TimerCnt>=TIME_WINDOW_MIN)&&(angl_TimerCnt<=TIME_WINDOW_MAX))
      {
        if ((((angl_sample_sum < angl_threshold)&&(angl_fgNegative==1))||
             ((angl_sample_sum > angl_threshold)&&(angl_fgPossitive==1)))&&
              (g_x_app*g_x_app+g_y_app*g_y_app+g_z_app*g_z_app >=0.25))//0.25
        {
          angl_TimerCnt_old[2] = angl_TimerCnt_old[1];
          angl_TimerCnt_old[1] = angl_TimerCnt_old[0];
          angl_TimerCnt_old[0] = angl_TimerCnt;
          STEPS=STEPS+1;

          angl_TimerCnt =  0;
          angl_fgPossitive = 0;
          angl_fgNegative= 0; 
        }
      }
   }
  }
  if(STEPS){
      delay_step2 = 0;
      //加入延时算法。
          if(delay_step1 < 6){
          delay_step1 += 1;
          tempStep += STEPS;
      }
      else{
          //ALLSTEPS += (STEPS+tempStep);
        for(uint8_t x =0;x<20;x++){
          arv_montion += g_sc_montion[x];
        }
        arv_montion = arv_montion/20;
        if (arv_montion <=2){//胸前补偿
          if((sec_clock%8)==1){
              tempStep += 1;
          }
        }
        UpdataCurrentData(STEPS+tempStep);
        tempStep = 0;
      }
  }
}

/*彩光通讯发送*/
void SendOutLightData(void){
  
  uint8_t V_bat;
    
//  if(BlockInfo.DataBlockNum == 0) return;
  
    //这里要把其他的所有中断都关闭。
    Switch_interrupter(MY_NO);
    //acc_interupt_counter = 120;//关闭中断不超过2分钟
    //擦除FLASH
    erase_area();
    
    //开EEPROM电源   
    GPIO_setOutputHighOnPin(V_EEPROM_PROT,
                            V_EEPROM_PIN);
    delay_ms(2);
    //=======做数据准备=============
    //encode(50,SEG_ADDR);//编码
    V_bat = (uint8_t)((AD_value - 3.0f) * 100);
    encodeLog(V_bat,SEG_ADDR);
 
    delay_ms(3);
    //关电源
    GPIO_setOutputLowOnPin(V_EEPROM_PROT,
                            V_EEPROM_PIN);
    
    timeB_couter = 0;
    val_i = 0;
    //清空变量
    Flash_ptr = (char *)SEG_ADDR;
    
    RGB_Light_ON(LEDGREEN);
    //开定时器，自动发。    
    TBCCTL0 = CCIE;                       
    //休眠
    LPM3;
    //关灯
    RGB_Light_AllOFF();
    //关闭定时器。
    TBCCTL0 &= ~CCIE;
        
    
    //清空变量
    Flash_ptr = (char *)SEG_ADDR;
    //开中断
    Switch_interrupter(MY_YES);
    //acc_interupt_counter = 6;
    
}

/*写数据指针*/
void updataBlockInfoToEEPROM(){
    
    union UN_VALUE {
      uint16_t value;
      uint8_t valByte[2];
    }un_value;
    
    un_value.value = BlockInfo.CurrentBlockAdrress;

    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR,un_value.valByte[0]);
    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+1,un_value.valByte[1]);
    
    un_value.value = BlockInfo.CutAdrress;
    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+2,un_value.valByte[0]);
    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+3,un_value.valByte[1]);
    
    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+4,BlockInfo.DataBlockNum);
    
}

/*读取数据指针*/
void reakBlockInfoFromEEPROM(){
    uint16_t value;
    uint8_t *valueL = (uint8_t *)(&value);
    uint8_t *valueH = ((uint8_t *)(&value)+1);
    
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR,valueL);
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+1,valueH);
    BlockInfo.CurrentBlockAdrress = value;
    
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+2,valueL);
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+3,valueH);
    BlockInfo.CutAdrress = value;
    
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+4,&BlockInfo.DataBlockNum);

}



/*将当前的CurrentData数据结构写入ROM*/
void WriteCurrentDataToEEPROM(){
  uint16_t i = 0;

  if(Standby_MODE != CurrentData.Mode){
    
    //先去掉尾部的00；
    if((DAY_MODE == CurrentData.Mode)&&
       (CurrentData.Data[CurrentData.DataLen-1]==0)&&
         ((CurrentData.DataLen-1) == 5)){
           return;
     }else{//只有非计步模式存在两个连续的零的现象。
       if((CurrentData.Data[CurrentData.DataLen-2]==0) && 
          (CurrentData.Data[CurrentData.DataLen-1]==0)){
            CurrentData.DataLen-=2;
       }
       if(CurrentData.Data[CurrentData.DataLen-1]==0){
            CurrentData.DataLen-=1;
       }
     }
     //如果本次计数数据持续是0,连模式字符都不要保存,直接退出。
     if((CurrentData.Data[CurrentData.DataLen-2]==0xfe) && 
        (CurrentData.Data[CurrentData.DataLen-3]==0xfe) &&
        ((CurrentData.DataLen-3) == 5)&&
         (DAY_MODE == CurrentData.Mode)){
          return ;
     }
    
     //如果本次计数开始就是0,连模式字符都不要保存,直接退出。
     if((CurrentData.DataLen) == 5){
          return ;
     }
     //去掉尾部的FEFE
     if((CurrentData.Data[CurrentData.DataLen-2]==0xfe) && 
        (CurrentData.Data[CurrentData.DataLen-3]==0xfe) &&
          ((CurrentData.DataLen-3)>5)&&
            (DAY_MODE == CurrentData.Mode)){
              CurrentData.DataLen -= 3;
     }
  }
  
  //开电源
  P2OUT |= 0x01; 
  delay_ms(2);
  
  for(i=0;i<CurrentData.DataLen;i++){
    M24C64_WriteByte_PowerON(BlockInfo.CurrentBlockAdrress + i,
                                           CurrentData.Data[i]);
    delay_ms(6);
  }
  delay_ms(6);
  //关电源
  P2OUT &= 0xfe; 
  BlockInfo.CurrentBlockAdrress+=CurrentData.DataLen;
  BlockInfo.DataBlockNum++;
  //============判断是否需要对EEPROM数据做循环使用处理================
  if((BlockInfo.CurrentBlockAdrress -FRIST_DATA_BLOCK_ADDR) >= MAX_LEN_DATA){
      
      arrangeEEPprt_Data(FRIST_DATA_BLOCK_ADDR,
                         BlockInfo.CutAdrress,
                         BlockInfo.CurrentBlockAdrress);
   
      BlockInfo.CurrentBlockAdrress = (BlockInfo.CurrentBlockAdrress - 
                                      BlockInfo.CutAdrress)+
                                      FRIST_DATA_BLOCK_ADDR;
      
      BlockInfo.CutAdrress = 0;
  
  }
  else if(((BlockInfo.CurrentBlockAdrress -FRIST_DATA_BLOCK_ADDR) >= MIN_LEN_DATA)
                       &&(BlockInfo.CutAdrress==0)){
       BlockInfo.CutAdrress = BlockInfo.CurrentBlockAdrress;
  }
  //更新EEPROM中数据结构表。
  updataBlockInfoToEEPROM();
}

/*更新当前的数据结构*/
//这个Num必须大于0,否者不需要调用本函数
void UpdataCurrentData(uint8_t Num){
  uint16_t w16 = 0;
    
//  if(0 == Num)
//    //本次采集出来的数据为0 ? get out!
//    return;
  if(DAY_MODE==CurrentData.Mode){
    ALLSTEPS += Num;
    w16 = CurrentData.Data[CurrentData.DataLen] & 0xFF;
    w16 <<= 8;
    //w16 += CurrentData.Data[CurrentData.DataLen +1];
    w16 |= CurrentData.Data[CurrentData.DataLen +1];
    
    w16 += Num;
    CurrentData.Data[CurrentData.DataLen] = (w16>>8);//高位
    CurrentData.Data[CurrentData.DataLen +1] = w16;//低位
  }
  else if(NIGHT_MODE==CurrentData.Mode){
    if(CurrentData.minutes < (MINUTES_CYCLE/2))
      CurrentData.Data[CurrentData.DataLen] += Num;
    else
      CurrentData.Data[CurrentData.DataLen+1] += Num;
  }
}

/*添加当前时间到FFFF模式标志符下面*/
void addTimeNow(){
  
    Calendar TimeNow;
    uint8_t time[5]={0};
    uint8_t T[3]={0};
    
    
    TimeNow = RTC_A_getCalendarTime(RTC_A_BASE);
    time[0] = TimeNow.Year - 2013;
    time[1] = TimeNow.Month;
    time[2] = TimeNow.DayOfMonth;
    time[3] = TimeNow.Hours;
    time[4] = TimeNow.Minutes;

    //T1
    T[0] = (time[0]<<4);//year 4
    T[0]  += time[1];//month 4
    //T2
    T[1] = (time[2]<<3);//day 5
    T[1]  += (time[3]>>2);//H 5>>2 = 3
    //T3
    T[2]  = ((time[3]<<6) & 0xc0);//H 2<<6 & (1100 0000)B
    T[2]  += time[4];//M 6
    
    M24C64_WriteByte((uint16_t)(LOG_PAGE_ADDR+4),T[0]);
    M24C64_WriteByte((uint16_t)(LOG_PAGE_ADDR+5),T[1]);
    M24C64_WriteByte((uint16_t)(LOG_PAGE_ADDR+6),T[2]);
} 

                           
/*新建一个CurrentData，注意：默认是白天模式
新建完成后，修改模式。
*/                        
void init_CurrentData(WorkMode mode){
    //char buffer[50];
    uint8_t i = 0;
    Calendar TimeNow;
    uint8_t time[5]={0};
    //char buffer[40];
    
//    CurrentData.minutes = 0;
    CurrentData.Mode = mode;
    CurrentData.ZeroNum = 0;
    
    for(i=5;i<255;i++){
      CurrentData.Data[i] = 0;
    }
    if(DAY_MODE == mode){
      CurrentData.Data[0] = 0xfc;
      CurrentData.Data[1] = 0xfc;
    }
    else if(NIGHT_MODE == mode){
      CurrentData.Data[0] = 0xfd;
      CurrentData.Data[1] = 0xfd;   
    }
    else if(Standby_MODE == mode){
      CurrentData.Data[0] = 0xff;
      CurrentData.Data[1] = 0xfe;
    }
    //把年月日时分5字节数组，整理成3个8位。
    
    TimeNow = RTC_A_getCalendarTime(RTC_A_BASE);

    //导入未进入睡眠状态之前的数据，需要将时间数据往前推MIN_TIME_INTO_SLEEP分钟

    if(NIGHT_MODE == mode){
      if(TimeNow.Minutes >= MIN_TIME_INTO_SLEEP){
        TimeNow.Minutes -= MIN_TIME_INTO_SLEEP;
        
      }
      else{
        TimeNow.Minutes += (60-MIN_TIME_INTO_SLEEP);
        if(TimeNow.Hours == 0){
           TimeNow.Hours =23;
           TimeNow.DayOfMonth = g_old_date;
        }
        else{
           TimeNow.Hours -= 1;
        }
      }
      //sprintf(buffer,"Goccia into sleep time is %d:%d",
      //                   TimeNow.Hours,TimeNow.Minutes);
      //Log(buffer);
    }
    time[0] = TimeNow.Year - 2013;
    time[1] = TimeNow.Month;
    time[2] = TimeNow.DayOfMonth;
    time[3] = TimeNow.Hours;
    time[4] = TimeNow.Minutes;
    
    //T1
    CurrentData.Data[2] = (time[0]<<4);//year 4
    CurrentData.Data[2] += time[1];//month 4
    //T2
    CurrentData.Data[3] = (time[2]<<3);//day 5
    CurrentData.Data[3] += (time[3]>>2);//H 5>>2 = 3
    //T3
    CurrentData.Data[4] = ((time[3]<<6) & 0xc0);//H 2<<6 & (1100 0000)B
    CurrentData.Data[4] += time[4];//M 6
    
    CurrentData.minutes = TimeNow.Minutes%10;
    //初始的数据就有5个了
    CurrentData.DataLen = 5;
    CurrentData.ZeroZip = false;
}

//零压缩
void zero_zip(uint8_t num){
  uint8_t temp =0;
    if(CurrentData.ZeroZip == true){
       CurrentData.Data[CurrentData.DataLen-1]=num;
    }
    else {
       temp = CurrentData.DataLen - (num - 1);
       CurrentData.Data[temp]=0xFE;
       CurrentData.Data[temp+1]=0xFE;
       CurrentData.Data[temp+2]=num;
       if(CurrentData.Mode==DAY_MODE){
           CurrentData.DataLen = temp+3;
       }
       CurrentData.ZeroZip = true;
    }
    
}

/*数据整形*/
void dataReshape(){
    uint16_t w16 = 0;
    
     if(CurrentData.Mode==DAY_MODE){
       //在这里做FCFC 8位/16位不定长数据的处理。根据协议5.30
        w16 = CurrentData.Data[CurrentData.DataLen] & 0xFF;
        w16 <<= 8;
        w16 += CurrentData.Data[CurrentData.DataLen +1];
        
        if (w16==0){
            CurrentData.ZeroNum ++;
            if(CurrentData.ZeroNum>=4){
               zero_zip(CurrentData.ZeroNum);
            }
            else{
               CurrentData.DataLen++;
            }
//            CurrentData.Data[CurrentData.DataLen]=0;
//            CurrentData.Data[CurrentData.DataLen+1]=0;
        }
        else{
            CurrentData.ZeroNum = 0;
            CurrentData.ZeroZip = false;
            if (w16 <= 254 ){
            CurrentData.Data[CurrentData.DataLen]= (w16/2) & 0x7f;
            CurrentData.Data[CurrentData.DataLen+1]= 0;
            CurrentData.DataLen++;
            }
            else{
                CurrentData.Data[CurrentData.DataLen + 1]= (w16 & 0x7f);
                CurrentData.Data[CurrentData.DataLen] = ((w16 >> 7) | 0x80);
                CurrentData.DataLen+=2;
            }
        }
     }
     else if(CurrentData.Mode==NIGHT_MODE){//睡眠模式
       uint8_t tem_val[2]={0};
       tem_val[0] = CurrentData.Data[CurrentData.DataLen];
       tem_val[1] = CurrentData.Data[CurrentData.DataLen+1];
       
       for(uint8_t i =0;i<2;i++){
         if (tem_val[i]==0){
            CurrentData.ZeroNum ++;
            if(CurrentData.ZeroNum>=4){
               zero_zip(CurrentData.ZeroNum);
               //数据进位
               if(i==0){
               CurrentData.Data[CurrentData.DataLen]=
                                CurrentData.Data[CurrentData.DataLen+1];
               }
            }
            else{
               CurrentData.DataLen++;
            }
         }else{
            CurrentData.ZeroNum = 0;
            CurrentData.ZeroZip = false;
            CurrentData.DataLen++;
         }
       }
     }
}
/*时间校准*/
void cal_time(uint8_t* data){
  
    Calendar currentTime;
    RTC_A_holdClock(RTC_A_BASE);
    RTC_A_disableInterrupt(RTC_A_BASE,RTCAIE);
    RTC_A_disableInterrupt(RTC_A_BASE,RTCTEVIE);
    RTC_A_disableInterrupt(RTC_A_BASE,RTCRDYIE);
    
    currentTime.Year       = 2013 + ((data[1]>>4)&0x0f);
    currentTime.Month      = data[1]&0x0f;
    currentTime.DayOfMonth =  ((data[2]>>3)&0x1F);
    g_old_date = currentTime.DayOfMonth;
    currentTime.DayOfWeek  = 2;
    currentTime.Hours      = (((data[2])&0x07)<<2)+(((data[3])>>6)&0x03);
    currentTime.Minutes    = ((data[3])&0x3f);
    currentTime.Seconds    = 0;
    
    RTC_A_calendarInit(RTC_A_BASE,
                       currentTime,
                       RTC_A_FORMAT_BINARY);
    
    //设置每分钟发生一次中断。
    RTC_A_setCalendarEvent(RTC_A_BASE,
                RTC_A_CALENDAREVENT_MINUTECHANGE);
    
    if(((currentTime.Hours>=SLEEP_TIME_H)&&(currentTime.Minutes>SLEEP_TIME_M))
       ||(currentTime.Hours<WAKEUP_TIME_H)){
        g_SleepDetect = ActiveSleepDetectOpen;
        RTC_A_setCalendarAlarm(RTC_A_BASE,WAKEUP_TIME_M,WAKEUP_TIME_H,//8:15
                     RTC_A_ALARMCONDITION_OFF,
                     RTC_A_ALARMCONDITION_OFF);
    }
    else{
        g_SleepDetect = ActiveSleepDetectClose;
        RTC_A_setCalendarAlarm(RTC_A_BASE,SLEEP_TIME_M,SLEEP_TIME_H,//22:10
                     RTC_A_ALARMCONDITION_OFF,
                     RTC_A_ALARMCONDITION_OFF);
    }

    //使能闹钟中断。
    RTC_A_enableInterrupt(RTC_A_BASE,RTCAIE);
    //使能分钟中断。
    RTC_A_enableInterrupt(RTC_A_BASE,RTCTEVIE);

    //Start RTC Clock
    RTC_A_startClock(RTC_A_BASE);
  
}
/*删除记录*/
void deleteRecord(void){
  
    BlockInfo.CurrentBlockAdrress = FRIST_DATA_BLOCK_ADDR;
    BlockInfo.DataBlockNum = 0;
    BlockInfo.CutAdrress = 0;
    motionAddress = MOTION_PAGE_ADDR;
    motionCutAddress = 0;
    creatMotionFlag();
    init_CurrentData(DAY_MODE);
    updataBlockInfoToEEPROM();
    updataMotionAddressToEEPROM();
    tempStep = 0;
}

/*插入运动量Flag*/
void creatMotionFlag(){
  
    Calendar TimeNow;
    uint8_t time[5]={0};
    uint8_t T[3]={0};
    
  //如果超过MIN_LEN_MOTION则设定切割点
    if((motionCutAddress==0)&&
     ((motionAddress - MOTION_PAGE_ADDR) >=MIN_LEN_MOTION)){
          motionCutAddress = motionAddress;
     }
    
    
    
    TimeNow = RTC_A_getCalendarTime(RTC_A_BASE);
    time[0] = TimeNow.Year - 2013;
    time[1] = TimeNow.Month;
    time[2] = TimeNow.DayOfMonth;
    time[3] = TimeNow.Hours;
    time[4] = TimeNow.Minutes;

    //T1
    T[0] = (time[0]<<4);//year 4
    T[0]  += time[1];//month 4
    //T2
    T[1] = (time[2]<<3);//day 5
    T[1]  += (time[3]>>2);//H 5>>2 = 3
    //T3
    T[2]  = ((time[3]<<6) & 0xc0);//H 2<<6 & (1100 0000)B
    T[2]  += time[4];//M 6
    
    M24C64_WriteByte(motionAddress++,0xFC);
    M24C64_WriteByte(motionAddress++,0xFD);
    M24C64_WriteByte(motionAddress++,T[0]);
    M24C64_WriteByte(motionAddress++,T[1]);
    M24C64_WriteByte(motionAddress++,T[2]);
    
    //===================================================
  //如果超过MAX_LEN_MOTION则开始切割
  if((motionAddress - MOTION_PAGE_ADDR) >=MAX_LEN_MOTION){
    
        arrangeEEPprt_Data(MOTION_PAGE_ADDR,
                 motionCutAddress,
                 motionAddress);
        motionAddress = (motionAddress - 
                        motionCutAddress)+
                        MOTION_PAGE_ADDR;
        motionCutAddress = 0;
     }
  updataMotionAddressToEEPROM();
  
}

/*写运动量数据指针*/
void updataMotionAddressToEEPROM(){
    
    union UN_VALUE {
      uint16_t value;
      uint8_t valByte[2];
    }un_value;
    
    un_value.value = motionAddress;

    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+10,un_value.valByte[0]);
    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+11,un_value.valByte[1]);
    
    un_value.value = motionCutAddress;
    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+12,un_value.valByte[0]);
    M24C64_WriteByte(FRIST_BLOCK_INFO_ADDR+13,un_value.valByte[1]);
    
    
}

/*读取运动量数据指针*/
void reakMotionAddressFromEEPROM(){
    uint16_t value;
    uint8_t *valueL = (uint8_t *)(&value);
    uint8_t *valueH = ((uint8_t *)(&value)+1);
    
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+10,valueL);
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+11,valueH);
    motionAddress = value;
    
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+12,valueL);
    M24C64_ReadByte(FRIST_BLOCK_INFO_ADDR+13,valueH);
    motionCutAddress = value;
    

}

/*运动量写入EEPROM*/
void addMotionValueToEEPROM(uint8_t montion_Value){
  
  uint8_t temp=0,temp2=0;
  if(motionAddress >= MOTION_PAGE_ADDR + 1000){
     motionCutAddress = 0;
     motionAddress = 0;
  }
  
  if(montion_Value>MOTION_OUT_LEVER){
    //比例计算
  if(montion_Value>250) montion_Value=250;
      M24C64_WriteByte(motionAddress++,montion_Value);
  }
  else {
    montion_Value = 0;
    M24C64_ReadByte(motionAddress-2,&temp);
    M24C64_ReadByte(motionAddress-1,&temp2);
    if(temp==0xfe){
       M24C64_ReadByte(motionAddress-1,&temp);
       if(temp<250){
         M24C64_WriteByte(motionAddress-1,temp+1);
       }
       else{
         M24C64_WriteByte(motionAddress++,0xfe);
         M24C64_WriteByte(motionAddress++,0xfe);
         M24C64_WriteByte(motionAddress++,1);
       }
    }
    else if((temp==0)&&(temp2==0)){
        M24C64_ReadByte(motionAddress-6,&temp);
        M24C64_ReadByte(motionAddress-5,&temp2);
        if((temp!=0xfc)&&(temp2!=0xfd)){
           M24C64_WriteByte(motionAddress-2,0xfe);
           M24C64_WriteByte(motionAddress-1,0xfe);
           M24C64_WriteByte(motionAddress++,3);
        }
        else{
           M24C64_WriteByte(motionAddress++,0);
        }
    }
    else{
       M24C64_WriteByte(motionAddress++,0);
    }
  }
  updataMotionAddressToEEPROM();
}


/*设置目标*/
void setTarget(){
   TagetStep = (screenByte[2] << 8) + (screenByte[3] & 0xFF);
   M24C64_WriteByte(TAGET_ADDR,screenByte[2]);
   M24C64_WriteByte(TAGET_ADDR+1,screenByte[3]);
}



/*整理EEPROM的数据，循环使用

将Cutaddr地址上的数据按地址递增写到Beginaddr，
第一个数据的原地址是Cutaddr，新地址是Beginaddr
最后一个数据原地址是Endaddr-1，新地址是Endaddr-Cutaddr+Beginaddr-1
-----------------------------------------------
Beginaddr |.....|Cutaddr|......|Endaddr
-----------------------------------------------
0xNN      |     |0xMM   |......|0xKK          原数据
-----------------------------------------------
0xMM      |....................|0xKK          新数据
*/
void arrangeEEPprt_Data(uint16_t Beginaddr,uint16_t Cutaddr,
                        uint16_t Endaddr){
  uint8_t buf[128];
  uint8_t num =0;
  uint8_t temp =0;
  uint16_t addr = Cutaddr;
  uint16_t wrtFlash_ptr = TEMP_SEG_ADDR;
  char *readFlash_ptr= (char *)TEMP_SEG_ADDR;
  
  if(Cutaddr <= Beginaddr) return;
  if(Endaddr <= Cutaddr) return;
  //擦FLASH
  __disable_interrupt();
  for(uint8_t i=0;i<3;i++){//中转区域大小为3*512=1.5K
     erase_Seg(TEMP_SEG_ADDR+(i*0x201));
  }
  
  //读第一page
  num=(PAGE_BYTES-addr%PAGE_BYTES);
  M24C64_ReadPageWithLen(addr,buf,num);
  wrt_flash_arry(wrtFlash_ptr,buf,num);
  wrtFlash_ptr+=num;
  
  //读中间整page
  addr += num;
  while(addr+128 <= Endaddr){
      M24C64_ReadPageWithLen(addr,buf,PAGE_BYTES);
      wrt_flash_arry(wrtFlash_ptr,buf,PAGE_BYTES);
      wrtFlash_ptr+=PAGE_BYTES;
      addr += PAGE_BYTES;
  }
  //读最后一个page
  num = Endaddr - addr;
  if(num){
    M24C64_ReadPageWithLen(addr,buf,num);
    wrt_flash_arry(wrtFlash_ptr,buf,num);
  }
  
  /*==========写回EEPROM==============*/
  //写前面整PAGE_BYTES，整页写。
  num = (Endaddr - Cutaddr)/PAGE_BYTES;
  for( temp=0;temp<num;temp++){
    for(uint8_t j=0;j<PAGE_BYTES;j++) buf[j] = *readFlash_ptr++;
    M24C64_WritePage(Beginaddr+(temp*PAGE_BYTES),buf);
  }
  //写最后的非整PAGE_BYTES的数据
  num = (Endaddr - Cutaddr)%PAGE_BYTES;
  if(num){
    for(uint8_t j=0;j<num;j++) buf[j] = *readFlash_ptr++;
    M24C64_WritePageWithLen(Beginaddr+(temp*PAGE_BYTES),buf,num);
  }
  
 // Endaddr = Endaddr - Cutaddr + Beginaddr;
  __enable_interrupt();
}
