
#include "led.h"
#include "core.h"

//
/*=====太长时间没运动提醒======
红色LED 亮，每分钟亮一次，每次亮5
下，每下100ms，间隔200ms。总共亮
30 分钟。中途如果测到步数，停止。
*/
void led_prolongedMotionless()
{
  int i = 0;
  for(i=0;i<5;i++){
    
      clock_light_color_AllOn(CLOCK_RED);
      delay_ms(100);
      clock_light_color_AllOff(CLOCK_RED);
      delay_ms(200);
    
  }

}


/* ====目标达到提醒========
蓝色LED 依次亮起，每个灯亮100ms，
占空比50%，前后两个灯亮的间隔
200ms.一个灯亮，之前的灯不熄灭。第
四个灯灭，全部灯一起灭。
*/
void led_reachGoal()
{
  int i = 0,j = 0;
  for(i=0;i<2;i++){
    for(j=1;j<=6;j++){
        clock_light_on(j);
        delay_ms(500);
        //clock_light_off(CLOCK_GREEN,j);
        //delay_ms(500);
    }
    clock_light_color_AllOff();
  }
}

/*====进入计步功能=======
彩色LED 中的红色灯亮。亮2 下，亮
300ms，停500ms。

*/

void led_switchToStepCounter()
{
  int i = 0;
  for(i=2;i!=0;i--)
  {
    RGB_Light_ON(LEDRED);
    delay_ms(200);
    RGB_Light_OFF(LEDRED);
    delay_ms(500);
  }
}

///*====进入睡眠监测功能=======
//彩色LED 中的绿色灯亮。亮2 下，亮
//300ms，停500ms。
//*/
//
//void led_switchToSleepMonitor()
//{ 
//  int i = 0;
//  for(i=2;i!=0;i--)
//  {
//    RGB_Light_ON(LEDBLUE);
//    delay_ms(200);
//    RGB_Light_OFF(LEDBLUE);
//    delay_ms(500);
//  }
//}

/*======充电中================
    电量0-25%，A 号灯亮500ms，灭500ms，
    （1Hz 闪烁）循环；
    电量25%-50%， A 灯常亮，B 号灯以1Hz
    闪烁；
    电量50%-75%，A,B 灯常亮，C 号灯以
    1Hz 闪烁；
    电量75%-100%，A,B,C 号灯常亮，D 号
    灯以1H 组闪烁。
*/

void led_charging()
{
  clock_light_color_AllOff();
  if(AD_value>4.15)//充满电了。99%
  {
    g_powerlever = 12;
  }
  else if(AD_value>4.12)//95%
  {
    g_powerlever = 11;
  }
  else if(AD_value>4.08)//90%
  {
    g_powerlever = 10;
  }
  else if(AD_value>3.97)//80%
  {
    g_powerlever = 9;
  }
  else if(AD_value>3.90)//70%
  {
    g_powerlever = 8;
  }
  else if(AD_value>3.84)//60%
  {
    g_powerlever = 7;
  }
  else if(AD_value>3.76)//42%
  {
    g_powerlever = 6;
  }
  else if(AD_value>3.74)//35%
  {
    g_powerlever = 5;
  }
  else if(AD_value>3.72)//25%
  {
    g_powerlever = 4;
  }
  else if(AD_value>3.69)//16%
  {
    g_powerlever = 3;
  }
  else if(AD_value>3.64)//8%
  {
    g_powerlever = 2;
  }
  else
  {
    g_powerlever = 1;
  }
  
//  for(uint8_t i =0;i<g_powerlever;i++)
//    clock_light_on(i);
}

void ledShow(){
    
  headerLED = 1;
  g_pwm_show = true;  
  timer_1ms_couter = 0;
  TA0CCTL0 = CCIE;
  
}

void ledShow_pwm(){
    //头灯，PWM 100%,接下来以20%递减
    /*阶梯式灭灯*/
    if(timer_1ms_couter%10==2){
       clock_light_color_AllOff();
       clock_light_on(headerLED);
       clock_light_on(headerLED-1);
       clock_light_on(headerLED-2);
       //clock_light_on(headerLED+1);
    }
    
    if(timer_1ms_couter%10==5){
       clock_light_off(headerLED-2);
    }
    if(timer_1ms_couter%10==7){
       clock_light_off(headerLED-1);
    }
    
    if(timer_1ms_couter%10==9){
       clock_light_off(headerLED);
    }

    /*亮灯*/
    if(timer_1ms_couter%10==0){
       clock_light_color_AllOn();
    }
    
    if(timer_1ms_couter%150==0){//100ms递进一个灯
      headerLED++;
      if(headerLED==13){
       headerLED = 1;
      }
    }
    if(timer_1ms_couter>=5000){                  
      clock_light_color_AllOff();                //全灭
      TA0CCTL0 &= ~CCIE;                         //关中断
      g_pwm_show = false;
      timer_1ms_couter = 0;
    }
}

//void chargeLED_pwm(){
//    /*阶梯式灭灯*/
//    if(timer_1ms_couter%10==3){
//       clock_light_color_AllOff();
//       clock_light_on(headerLED);
////       clock_light_on(headerLED-1);
////       clock_light_on(headerLED+1);
//    }
//    
////    if(timer_1ms_couter%10==5){
////       clock_light_off(headerLED-1);
////       clock_light_off(headerLED+1);
////    }
////    
//    if(timer_1ms_couter%10==9){
//       clock_light_off(headerLED);
//    }
//
//    /*亮灯*/
//    if(timer_1ms_couter%10==0){
//      for(uint8_t i=0;i<=g_powerlever;i++){
//       clock_light_on(i);
//      }
//    }
//    
//    if(timer_1ms_couter%300==0){//100ms递进一个灯
//      headerLED++;
//      if(headerLED==g_powerlever+1){
//       headerLED = 1;
//      }
//    }
//    if(timer_1ms_couter>=2400){                  //电量只显示半秒。
//      clock_light_color_AllOff();                //全灭
//      TA0CCTL0 &= ~CCIE;                         //关中断
//      g_pwm_show = false;
//      timer_1ms_couter = 0;
//    }
//}


//点亮指定的颜色的RGB灯
void RGB_Light_ON(uint8_t Color)
{
    GPIO_setOutputHighOnPin(LED_RGB_PORT,
                            LED_R_PIN+LED_G_PIN+LED_B_PIN);
	
    GPIO_setOutputLowOnPin(LED_RGB_PORT,
                            Color);
}

//关闭指定的颜色的RGB灯
void RGB_Light_OFF(uint8_t Color)
{
     GPIO_setOutputHighOnPin(LED_RGB_PORT,Color);

}
//关闭所有颜色的RGB灯
void RGB_Light_AllOFF()
{
    GPIO_setOutputHighOnPin(LED_RGB_PORT,
                            LED_R_PIN+LED_G_PIN+LED_B_PIN);
}


//翻转指定的四周的灯。可叠加
void clock_light_toggle(uint8_t pos)
{
    switch(pos){
        
        case 1:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT1,
                                    CLOCK_LED_PIN1);
          break;
        case 2:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT2,
                                      CLOCK_LED_PIN2);
          break;
        case 3:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT3,
                                      CLOCK_LED_PIN3);
          break;
        case 4:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT4,
                                      CLOCK_LED_PIN4);
          break;
        case 5:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT5,
                                      CLOCK_LED_PIN5);
          break;
        case 6:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT6,
                                      CLOCK_LED_PIN6);
          break;
        case 7:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT7,
                                      CLOCK_LED_PIN7);
           break; 
        case 8:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT8,
                                      CLOCK_LED_PIN8);
          break;
        case 9:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT9,
                                      CLOCK_LED_PIN9);
          break;
        case 10:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT10,
                                      CLOCK_LED_PIN10);
          break;
        case 11:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT11,
                                      CLOCK_LED_PIN11);
          break;
        case 12:
            GPIO_toggleOutputOnPin(CLOCK_LED_PORT12,
                                      CLOCK_LED_PIN12);
          break;

       }
}

//点亮指定的四周的灯。可叠加
void clock_light_on(uint8_t pos)
{ 
   switch(pos){
        
        case 1:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT1,
                                    CLOCK_LED_PIN1);
          break;
        case 2:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT2,
                                      CLOCK_LED_PIN2);
          break;
        case 3:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT3,
                                      CLOCK_LED_PIN3);
          break;
        case 4:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT4,
                                      CLOCK_LED_PIN4);
          break;
        case 5:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT5,
                                      CLOCK_LED_PIN5);
          break;
        case 6:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT6,
                                      CLOCK_LED_PIN6);
          break;
        case 7:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT7,
                                      CLOCK_LED_PIN7);
           break; 
        case 8:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT8,
                                      CLOCK_LED_PIN8);
          break;
        case 9:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT9,
                                      CLOCK_LED_PIN9);
          break;
        case 10:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT10,
                                      CLOCK_LED_PIN10);
          break;
        case 11:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT11,
                                      CLOCK_LED_PIN11);
          break;
        case 12:
            GPIO_setOutputHighOnPin(CLOCK_LED_PORT12,
                                      CLOCK_LED_PIN12);
          break;

       }
}

//关闭指定的四周的灯。可叠加
void clock_light_off(uint8_t pos)
{
  
    switch(pos){
        
        case 1:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT1,
                                    CLOCK_LED_PIN1);
          break;
        case 2:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT2,
                                      CLOCK_LED_PIN2);
          break;
        case 3:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT3,
                                      CLOCK_LED_PIN3);
          break;
        case 4:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT4,
                                      CLOCK_LED_PIN4);
          break;
        case 5:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT5,
                                      CLOCK_LED_PIN5);
          break;
        case 6:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT6,
                                      CLOCK_LED_PIN6);
          break;
        case 7:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT7,
                                      CLOCK_LED_PIN7);
           break; 
        case 8:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT8,
                                      CLOCK_LED_PIN8);
          break;
        case 9:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT9,
                                      CLOCK_LED_PIN9);
          break;
        case 10:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT10,
                                      CLOCK_LED_PIN10);
          break;
        case 11:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT11,
                                      CLOCK_LED_PIN11);
          break;
        case 12:
            GPIO_setOutputLowOnPin(CLOCK_LED_PORT12,
                                      CLOCK_LED_PIN12);
          break;

       }
}


//点亮指定颜色全部的灯
void clock_light_color_AllOn()
{

   GPIO_setOutputHighOnPin(CLOCK_LED_PORT1,
                              CLOCK_LED_PIN1);

    GPIO_setOutputHighOnPin(CLOCK_LED_PORT2,
                              CLOCK_LED_PIN2);

    GPIO_setOutputHighOnPin(CLOCK_LED_PORT3,
                              CLOCK_LED_PIN3);

    GPIO_setOutputHighOnPin(CLOCK_LED_PORT4,
                              CLOCK_LED_PIN4);

    GPIO_setOutputHighOnPin(CLOCK_LED_PORT5,
                              CLOCK_LED_PIN5);

    GPIO_setOutputHighOnPin(CLOCK_LED_PORT6,
                              CLOCK_LED_PIN6);

    GPIO_setOutputHighOnPin(CLOCK_LED_PORT7,
                              CLOCK_LED_PIN7);

    GPIO_setOutputHighOnPin(CLOCK_LED_PORT8,
                              CLOCK_LED_PIN8);
    
    GPIO_setOutputHighOnPin(CLOCK_LED_PORT9,
                              CLOCK_LED_PIN9);
    
    GPIO_setOutputHighOnPin(CLOCK_LED_PORT10,
                              CLOCK_LED_PIN10);
    
    GPIO_setOutputHighOnPin(CLOCK_LED_PORT11,
                              CLOCK_LED_PIN11);
    
    GPIO_setOutputHighOnPin(CLOCK_LED_PORT12,
                              CLOCK_LED_PIN12);
  
  
}

//关闭全部的时钟灯
void clock_light_color_AllOff()
{

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT1,
                              CLOCK_LED_PIN1);

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT2,
                              CLOCK_LED_PIN2);

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT3,
                              CLOCK_LED_PIN3);

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT4,
                              CLOCK_LED_PIN4);

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT5,
                              CLOCK_LED_PIN5);

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT6,
                              CLOCK_LED_PIN6);

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT7,
                              CLOCK_LED_PIN7);

    GPIO_setOutputLowOnPin(CLOCK_LED_PORT8,
                              CLOCK_LED_PIN8);
    
    GPIO_setOutputLowOnPin(CLOCK_LED_PORT9,
                              CLOCK_LED_PIN9);
    
    GPIO_setOutputLowOnPin(CLOCK_LED_PORT10,
                              CLOCK_LED_PIN10);
    
    GPIO_setOutputLowOnPin(CLOCK_LED_PORT11,
                              CLOCK_LED_PIN11);
    
    GPIO_setOutputLowOnPin(CLOCK_LED_PORT12,
                              CLOCK_LED_PIN12);
}

void showProgress(){
  
  uint16_t temp =0;
  uint16_t tempAllsteps = ALLSTEPS;
 
   showTime();
   delay_ms(500);
  //低电压警报
  if(AD_value <= LOW_VOLTAGE_TO_ALM)
  {   
      //g_rgbLED_pwm = LED_R_PIN;
      P6OUT ^= LEDRED;
      delay_ms(200);
      P6OUT ^= LEDRED;
      delay_ms(200);
  }
//  else{
//        g_rgbLED_pwm = LED_G_PIN;
//        g_pwm_on =true;
//        P6OUT &= (~g_rgbLED_pwm);     
//        timer_1ms_couter = 0;
//        TA0CCTL0 = CCIE;
////      P6OUT ^= LEDGREEN;
////      delay_ms(200);
////      P6OUT ^= LEDGREEN;
////      delay_ms(200);
//  }

  if(tempAllsteps>=TagetStep)
    temp=12;
  else if(tempAllsteps>=(11*(TagetStep/12)))
    temp=11;
  else if(tempAllsteps>=(10*(TagetStep/12)))
    temp=10;
  else if(tempAllsteps>=(9*(TagetStep/12)))
    temp=9;
  else if(tempAllsteps>=(8*(TagetStep/12)))
    temp=8;
  else if(tempAllsteps>=(7*(TagetStep/12)))
    temp=7;
  else if(tempAllsteps>=(6*(TagetStep/12)))
    temp=6;
  else if(tempAllsteps>=(5*(TagetStep/12)))
    temp=5;
  else if(tempAllsteps>=(4*TagetStep/12))
    temp=4;
  else if(tempAllsteps>=(3*(TagetStep/12)))
    temp=3;
  else if(tempAllsteps>=(2*(TagetStep/12)))
    temp=2;
  else 
    temp=1;
  for(uint8_t j=1;j<=2;j++){
    for(uint8_t i=1;i<=temp;i++){
      clock_light_on(i);
      delay_ms(100);
    }
    delay_ms(100);
    clock_light_color_AllOff();
    delay_ms(200);
  }
 
  __disable_interrupt();
  LIS3DH_MutilReadFIFO( LIS3DH_FIFO_START,
                AxesBuffer,
                g_fifo_watrMark_lever);
  __enable_interrupt();
}

void showTime()
{
  Calendar TimeNow;
  uint8_t h;
  uint8_t m;
  uint8_t i;
  
  TimeNow = RTC_A_getCalendarTime(RTC_A_BASE);
  h = TimeNow.Hours;
  m = TimeNow.Minutes;
  
  if ( h > 12) h -= 12;  
  m = (m + 5/2) / 5;
  if(m==12) h+=1;
  if(m==0) m=12;
  if(h==0) h=12;
 
  clock_light_color_AllOff();
  clock_light_on(12);
  clock_light_on(h);
  
  for (i = 1; i < h; i++)
  {
    clock_light_on(i);
    delay_ms(100);
  }
  
  for (i = 1; i < h; i++)
  {
    clock_light_off(i);
    delay_ms(100);
  }
  delay_ms(500);
  // for minute
  for (i = 8; i != 0; i-- )
  {
    clock_light_on(m);
    delay_ms(300);
    clock_light_off(m);
    delay_ms(300);
  }
  delay_ms(500);
  // show off
  clock_light_color_AllOff();
}

