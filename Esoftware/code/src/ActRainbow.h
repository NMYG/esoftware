#ifndef __ACT_RAINBOW__H
#define __ACT_RAINBOW__H

#include <driverlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "lis3dh_driver.h"
#include "clock.h"
#include "bsp.h"
#include "m24c64.h"

//==========程序版本号=================
#define VERSION 67  
//==========断言相关操作=================
#define assert_param(expr) ((expr) ? \
  (void)0 : \
  assert_failed((uint8_t *)__FILE__, __LINE__));
// 软件定时相关操作
#define F_CPU ((double)8000000) 
#define delay_us(x) __delay_cycles((long)(F_CPU*(double)x/1000000.0)) 
#define delay_ms(x) __delay_cycles((long)(F_CPU*(double)x/1000.0))

//======硬件IO定义======================
/*=============外部中断输入==========*/
#define LIS3DH_INT2_PIN    GPIO_PIN0  //加速计中断2
#define LIS3DH_INT1_PIN    GPIO_PIN1  //加速计中断1
#define FLASH_LIGHT_PIN    GPIO_PIN2  //闪光灯PhotoDido
#define DATA_LIGHT_PIN     GPIO_PIN3  //数据PhotoDido
#define BAT_CHARGE_PIN     GPIO_PIN4  //充电指示

/*=============EEPROM 电源==========*/
#define V_EEPROM_PROT  GPIO_PORT_P2
#define V_EEPROM_PIN   GPIO_PIN0

/*=============数据photodiode电源==========*/
#define V_DATA_PHOTODIODE_PORT GPIO_PORT_PJ
#define V_DATA_PHOTODIODE_PIN  GPIO_PIN3

/*=============无线充电检测==========*/
#define V_CHG_PORT GPIO_PORT_P1
#define V_CHG_PIN GPIO_PIN4

/*=============RGB LED==========*/
#define LED_RGB_PORT GPIO_PORT_P6
#define LED_R_PIN GPIO_PIN0
#define LED_G_PIN GPIO_PIN1
#define LED_B_PIN GPIO_PIN2
/*=============RGB LED==========*/
#define UART_PORT GPIO_PORT_P4
#define UART_RX_PIN GPIO_PIN4
#define UART_TX_PIN GPIO_PIN5

/*=============四周绿光=========*/

#define CLOCK_LED_PORT12  GPIO_PORT_PJ
#define CLOCK_LED_PIN12   GPIO_PIN0

#define CLOCK_LED_PORT11  GPIO_PORT_P5
#define CLOCK_LED_PIN11   GPIO_PIN2

#define CLOCK_LED_PORT10  GPIO_PORT_P3
#define CLOCK_LED_PIN10   GPIO_PIN3

#define CLOCK_LED_PORT9 GPIO_PORT_P4
#define CLOCK_LED_PIN9  GPIO_PIN7

#define CLOCK_LED_PORT8 GPIO_PORT_P4
#define CLOCK_LED_PIN8 GPIO_PIN6

#define CLOCK_LED_PORT7 GPIO_PORT_P3
#define CLOCK_LED_PIN7 GPIO_PIN1

#define CLOCK_LED_PORT6 GPIO_PORT_P3
#define CLOCK_LED_PIN6 GPIO_PIN0

#define CLOCK_LED_PORT5 GPIO_PORT_P2
#define CLOCK_LED_PIN5 GPIO_PIN7

#define CLOCK_LED_PORT4 GPIO_PORT_P6
#define CLOCK_LED_PIN4 GPIO_PIN6

#define CLOCK_LED_PORT3 GPIO_PORT_P6
#define CLOCK_LED_PIN3 GPIO_PIN3

#define CLOCK_LED_PORT2 GPIO_PORT_P2
#define CLOCK_LED_PIN2 GPIO_PIN5

#define CLOCK_LED_PORT1 GPIO_PORT_P2
#define CLOCK_LED_PIN1 GPIO_PIN6
 
/*=============分压电路地=========*/
#define G_AD_VBAT_PORT GPIO_PORT_PJ
#define G_AD_VBAT_PIN GPIO_PIN1


//#define RGB_OUT  P6OUT

//=====软件数值宏定义===============
//#define FIFO_WATERMAKR_LEVER    25
#define G_SCALE                 4
#define MINUTES_CYCLE           10  //2字节数据包含的分钟数 必须大于等于2
//#define BID1   0x0000            //之后要写入FLASH
//#define BID2   0x1251

//#define DEBUG_LIGHT   
#define IAP_ON 

//=====枚举类定义========================
typedef enum {
  UnActive_MODE =       0x03,    //为激活模式
  Standby_MODE  =       0x02,   //待机模式，最低功耗
  DAY_MODE	=	0x01,   //计步模式
  NIGHT_MODE	=	0x00	//睡眠模式
} WorkMode;

typedef enum {
  MY_YES	=	0x01,
  MY_NO 	=	0x00	
} MY_FLAG;

typedef enum {
  FLAG_Nothing          = 0x00, //无事件
  FLAG_WaterMarkFull	= 0x01, //FIFO满
  FLAG_Activety         = 0x02, //激活、时间校准事件
  FLAG_FlashLight       = 0x03, //闪光灯事件
  FLAG_WirlessCharge    = 0x04, //无线充电事件
  FLAG_LowPower         = 0x05,  //低电压事件
  FLAG_intoStandby      = 0x06, //待机指令
  FLAG_OutOfStandby     = 0x07,  //屏幕光通讯指令
  FLAG_IAP              = 0x08
} EventFlag;

typedef enum{
  ActiveSleepDetectOpen  = 0x00, //进入主动睡眠侦测
  ActiveSleepDetectClose = 0x01, //关闭主动睡眠侦测
  DeepSleeping           = 0x02 //进入深度睡眠模式
}StatuFlag;

//=====全局变量定义======================
extern uint8_t g_fifo_watrMark_lever;
extern volatile uint16_t timeB_couter;//timeB计数
extern int val_j ,val_k ,val_i ;//中断里面的循环要放全局中断
extern char *Flash_ptr;
extern EventFlag volatile event_flag;
extern AxesRaw_t AxesBuffer[30];
//extern volatile MY_FLAG flashStartFlag;
extern volatile Calendar newTime;
extern volatile uint8_t RTC_1s_couter;
extern volatile uint16_t timer_1ms_couter;//1毫秒定时计数
extern volatile uint16_t timer_1ms_TimeUp;//1秒超时
extern volatile uint16_t timer_20us_couter;//20微秒定时计数
extern volatile uint16_t reSend_couter;//重发次数
extern uint8_t g_powerlever;//电量等级
extern float AD_value;
extern StatuFlag g_SleepDetect; 
//extern uint16_t g_eeprom_adr;
extern void do_lightJob();
extern uint16_t sec_clock;
extern uint8_t screenByte[4];//接收到的屏幕光的数据
extern uint16_t bitCounter;
extern char ScreenHeaderFlag;
extern volatile uint8_t Flash_flag;
extern uint8_t unActiveMinutes;
#endif

