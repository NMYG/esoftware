  #ifndef __CORE__H
  #define __CORE__H

  #include "ActRainbow.h"
  #include <driverlib.h>
  #include <math.h>

/*=============运动量宏定义===========*/
#define MOTION_OUT_LEVER 30//80

/*=================睡眠算法的宏定义===================*/

//#define MOTION_MONITOR_MODE      //完全的运动量检测。
//#define  UART_ON                //串口通信打开            
//#define LCD_DATA_OPEN

#define TIME_INTO_SLEEP         40//运动量数组的大小

#define MIN_TIME_INTO_SLEEP     30//符合睡眠状态的时间。必须小于等于TIME_INTO_SLEEP

#define AVR_MOTION_TO_SLEEP     3//平均每分钟内的运动量。
#define MIN_MOTION_TO_SLEEP     10//计算的时间内不允许超过的运动量。20-30  28
#define MIN_MOTION_TO_SLEEP_5M  15
#define MOTION_OUTTO_SLEEP      35//过去的5分钟运动数据平均超过此数值退出。

#define MAX_MOTION_OF_SLEEP     170//胸部佩戴为100

#define FIRST_MODE              DAY_MODE//DAY_MODE//NIGHT_MODE

//<参数>开始与停止自动睡眠检测的时间
#define SLEEP_TIME_H            22  //22:20
#define SLEEP_TIME_M            20

#define WAKEUP_TIME_H           8  //8:15
#define WAKEUP_TIME_M           15

#define TIME_TO_STANDBY_MODE    90//90分钟

//休眠电压
#define LOW_VOLTAGE_TO_SBM      3.5
#define LOW_VOLTAGE_TO_IAP      3.55

//低电压警报
#define LOW_VOLTAGE_TO_ALM      3.63

//重新启动不更改时间。用户看门狗重启。
#define CAL_TIME_WHEN_REBOOT 

//烧录时的时间
#define NOW_YEAR                2014   //年
#define NOW_DOW                 3      //星期
#define NOW_Y                   1      //月
#define NOW_D                   1      //日
#define NOW_H                   8      //时
#define NOW_M                   20      //分
#define NOW_S                   00     //秒

//#define UART_ON 1

typedef struct{
  uint8_t Value_zt;        
  uint8_t Value_zd;        //倒置
  uint8_t Value_zu;        //平置
  uint8_t Value_zz;        //侧置
}Z_AXIS_VAULE;

typedef enum {
  AXIS_X	=	1,
  AXIS_Y 	=	2,
  AXIS_Z 	=	3
} MY_AXIS;

typedef enum {
  AR_YES	=	0x01,
  AR_NO 	=	0x00
} MY_BOOL;

typedef enum {
  flag_ZT 	=	0x00,//倾斜
  flag_ZD	=	0x01,//倒置
  flag_ZU 	=	0x02,//正置
  flag_ZZ 	=	0x03//侧置
} Z_FLAG;  

typedef struct{
  WorkMode Mode;        //当前的工作模式
  uint8_t ZeroNum;         //现在数据中连续的0的个数（当检查到ZeroNum大于3时，
                        //在Data[]中写入0xFEFE +ZeroNum ,遇到非0，ZeroNum归零） 
  uint8_t DataLen;         //当前数据的个数,含模式与时间。
  uint8_t minutes;         //表示当前的分钟数
  bool ZeroZip;         //表示现在是否正处在0压缩状态
  uint8_t Data[255];       //
  /*数据只有在以下几种情况下才写入ROM
    1、切换模式 2、电池电量低 3、即将进入待机模式 4、开始同步数据 5、屏幕光通信 6、充电开始*/
}CurrentData_def;
  
//EEPROM存储情况数据结构
typedef struct{
  uint16_t CurrentBlockAdrress;  //当前可用块的地址。
  uint16_t CutAdrress;           //预切割的地址头，该地址数据被提到FRIST_DATA_BLOCK_ADDR
  uint8_t DataBlockNum;          //数据的个数
}BlockInfo_Def;

//屏幕光通信命令结构，如果屏幕光通信成功率高，则校验留做升级使用。
typedef enum {
    SC_TIME             = 0xF1,         //时间校准命令
    SC_TAGET            = 0xF3,         //设置运动目标命令
    /*以下保留做为参数传输使用*/
    SC_ERASE            = 0xF5,         //设置运动目标命令
    SC_TEST             = 0xF0,         //测试用命令，同步时间不激活
    SC_POS              = 0xF6,         //佩戴位置
//    TIME       = 0x06,  //时间校准命令
//    ERROR      = 0x07,  //设置运动目标命令
    END          = 0xAA   //结束
} ScreenCMD;

//typedef struct{
//    MY_BOOL     screenHeader;//数据头
//    uint8_t        Len;         //数据长度（数据域的长度）
//    ScreenCMD   Command;     //命令模式
//    uint8_t        CheckSum;    //校验值
//    uint8_t*       ScrData;     //数据
//}SCREEN_DATA;

typedef enum {
  SUCCESS       = 0x01,
  ERROR         = 0x00
} resualt_t;

extern uint32_t G_Value;

//工作指示灯的闪动周期，3秒的倍数
extern uint8_t lightOnCycle;

extern BlockInfo_Def    BlockInfo;
extern CurrentData_def  CurrentData;
//extern char temp_buffer[50]; 

extern uint32_t   ALLSTEPS;
extern unsigned int TagetStep;
extern uint16_t logAddress;
extern volatile uint16_t motionAddress;
extern uint16_t  motionCutAddress;

extern uint16_t montion_lever_1m[TIME_INTO_SLEEP];
//extern float montion_lever_3s[20];

//extern float Viberate_Value_3s[30];
//extern uint8_t lcd_time[64];
//extern float g_vibrate_lever;
extern uint8_t vib_detect_on;
extern uint16_t G_montion_Value;
extern Z_AXIS_VAULE  z_axis_value;
extern uint8_t g_old_date;


//resualt_t ActRainbow_NightRoutine(void);
resualt_t ActRainbow_FIFORoutine(void);
resualt_t ActRainbow_SwitchMode(void);
//resualt_t ActRainbow_SleepMonitor(void);
resualt_t ActiveWorkMode(void);
resualt_t SleepMonitorMode(void);
void StepCountHandler(void);
void SendOutLightData(void);
void ScreenDataTransmition(void);
void init_CurrentData(WorkMode mode);
void reakBlockInfoFromEEPROM(void);
void updataBlockInfoToEEPROM(void);
void UpdataCurrentData(uint8_t Num);
void ReadAllDataFromEEPROM(uint8_t *dataArr);
void WriteCurrentDataToEEPROM(void);
void dataReshape(void);
void enterChargingMode(void);
void Log(char* msg);
void dataReceiveOff(void);
void cal_time(uint8_t* data);
void deleteRecord(void);
void setArgument(uint8_t* data);
void intoStandbyMode();
void OutOfStandbyMode();
void addTimeNow();
void creatMotionFlag();
void setTarget();
void addMotionValueToEEPROM(uint8_t montion_Value);
void attitude_Montion_Cal();
void data_loop();
//void WaitTurnOver();
float XYStandard_Deviation(MY_AXIS);
void unActiveMode();
void arrangeEEPprt_Data(uint16_t Beginaddr,
                        uint16_t Cutaddr,
                        uint16_t Endaddr);

void updataMotionAddressToEEPROM();
void reakMotionAddressFromEEPROM();
#endif 