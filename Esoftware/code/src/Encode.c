/*
 * encode.c
 *
 *  Created on: 2013-9-10
 *  Author: SonicZY
*  Version: 5.22
 */

#include "ActRainbow.h"
#include "encode.h"
#include "FLASH_EEPROM.h"
#include "core.h"
#include "bsp.h"

uint8_t seg = 0;//段数

/************************************************************
  encode(const uint8_t powerLever,const uint16_t FlashAddress)
         

  根据RGBW光通信编码V5.21F协议，将从eeprom里读出得数据进行编码。
   输入参数
    uint16_t powerLever： 当前电量
    uint16_t FlashAddress 用于存储编码后的数据的FLASH起始地址
   
   输出：
    由于MCU RAM不够大
    输出的dataBand的一维数组直接存储在FLASH或EEPROM里。发送时直接
    从FLASH或EEPROM里一段一段（每段48个色带）的取出并发送。


    每段的黑条纹放到发送的时候去处理。
    0xFB在写EEPROM的时候处理。
************************************************************/

/*函数中直接定义数字的有些需要改成宏定义。 */
void encodeLog(const uint8_t powerLever,const uint16_t FlashAddress){
    uint8_t BID[4]={0};
    uint8_t i = 0,k = 0,n =0; //m = 0;
//    uint8_t tempValue = 0;
    uint16_t DataLen =0,DataLen1=0;
    uint16_t adr = 0;

    //一天的数据：24*6*2*8/80=28.8->30(约等30段)
    uint8_t dataByte[BYTE_NUM];//包含index & checkSum
    uint8_t lastSegBytes =0;//最后一段剩余的字节数。
    DataLen1 = BlockInfo.CurrentBlockAdrress - FRIST_DATA_BLOCK_ADDR;

    DataLen = DataLen1 + 5 + (motionAddress - MOTION_PAGE_ADDR);

    if(DataLen > (200*BYTE_NUM)){//不能大于170帧数据
      return;
    }
    i=9;// i=8加入了BID,i=9加入了VER
    
    addTimeNow();//添加当前时间。
    
    adr = FRIST_DATA_BLOCK_ADDR;

    lastSegBytes = (DataLen+2)%8;//计算最后剩几个byte
    //seg = ((DataLen/8)+2);//计算段数
    seg=((DataLen+2)/8)+2;
    if(0==lastSegBytes){ seg--; lastSegBytes = 8;}
    if(seg<20){reSend_couter = ((100+seg/2)/seg);}else{reSend_couter = 5;}
    for (k=0; k < seg; k++){
      for(n=0;n<BYTE_NUM;n++){
        dataByte[n] = 0;
      }
      
      if(k == 1){
        dataByte[1] = (TagetStep>>8)&0xff;
        dataByte[2] = TagetStep & 0xff;
        i = 3;//第二行开始 i从3开始填充
      } 
      for(;i<BYTE_NUM-1;i++){//第11位为 checkSum 位留着后面填充
          
        //到达数据末尾，进入log的数据。
          if(adr == BlockInfo.CurrentBlockAdrress)
            adr = MOTION_PAGE_ADDR;
          if(adr == motionAddress)
            adr =  LOG_PAGE_ADDR + 2;  
    
        if((k==(seg-1))&&(i == (lastSegBytes+1)))
            break;
          M24C64_ReadByte(adr++,&dataByte[i]);
          //M24C64_ReadByte_PowerON
      }
      i = 1;//第3行开始 i从1开始填充
      if(k == 0){//第一行要特殊处理
        //填充index
        dataByte[0] = 0;
        //填充N1
        dataByte[1]= seg - 1 ;//0x00-0ff
        //填充N2
        dataByte[2]= lastSegBytes * 8;//最后有效的bit数。
        //if(seg==1) dataByte[2] = 80;
        //填充P
        dataByte[3] = powerLever;
        //BID
          //读取ID
        BID[3] = (*((uint8_t *)0x1800)) & 0xFF;
        //判断是否有合法的ID
        if(BID[3]==0xff){
          BID[0]=0;
          BID[1]=0;
          BID[2]=0;
          BID[3]=1;
        }else{
          BID[3] = (*((uint8_t *)0x1800)) & 0xFF;
          BID[2] = (*((uint8_t *)0x1801)) & 0xFF;
          BID[1] = (*((uint8_t *)0x1802)) & 0xFF;
          BID[0] = (*((uint8_t *)0x1803)) & 0xFF;
        }
        dataByte[4] = (BID[0]&0x0f);
        dataByte[5] = BID[1];
        dataByte[6] = BID[2];
        dataByte[7] = BID[3];
        if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
          dataByte[8] = VERSION;
        }else{
          dataByte[8] = (VERSION |0x80) ;
        }
      }else{
        //填充index
        dataByte[0] = k;
      }
        //计算校验位
      for(n=0;n<BYTE_NUM-1;n++){
         dataByte[BYTE_NUM-1] ^= dataByte[n];
      }
      dataByte[BYTE_NUM-1] = ~dataByte[BYTE_NUM-1];
      
    wrt_flash_arry(FlashAddress+(k*BYTE_NUM),dataByte,BYTE_NUM);
    
  }  
}


