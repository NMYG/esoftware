//#include <driverlib.h>
//#include <gpio.h>
//#include "inc/hw_regaccess.h"
//
//#define OPEN_W()  P6OUT &= (~0x07)//open
//#define CLOSE_W()  P6OUT |=0x07//close
//
//#define UUID_ADR  0x8600
//
//#define IAP_YES                 1
//#define IAP_NO                  0
//#define PACK_DATA_LEN           256
//
//typedef struct{
//    uint8_t indexNum;
//    uint8_t data[PACK_DATA_LEN];  
//    uint16_t lenth;
//    uint16_t addr;
//    uint16_t CRC_Value;
//}UpdateData_def;
//
//void wrt_flash_arryIAP(uint16_t addr,uint8_t* data,uint8_t len);
//
//
//void Delay(int16_t TimingDelay)@"UPDATECODE"
//{
//  while (TimingDelay != 0x00)
//  { 
//    for(int16_t i=19;i!=0;i--) _NOP();
//    TimingDelay--;
//  }
//}
//
//void sendArry(uint8_t* data,uint8_t len)@"UPDATECODE"
//{
//  uint8_t tempValue,index,num=0;
//  index=0;
//  tempValue=data[0];
//    while(index<len){
//      if((index==0)&&(num == 0)){
//          OPEN_W();
//          Delay(3000);//30ms
//          CLOSE_W();
//          Delay(500);
//      }
//       //data
//      OPEN_W();	
//      if((tempValue>>num)&0x01)
//      {
//         Delay(400);//1--->4ms
//      }
//      else
//      {
//         Delay(1500);//0--->15ms
//      }
//      CLOSE_W();
//      num++;
//      if(num==8) {
//        index++;
//        num = 0;
//        tempValue=data[index];
//      }
//      Delay(1000);//low -->10ms
//  }
//}
//
///*本函数有两个出口：1、升级成功，重启
//                   2、升级不成功，超时，返回main
//*/
//__monitor int updateCode()@"UPDATECODE"
//{
//  uint8_t sendchar[10];//8+1
//  uint16_t num = 0;
//  uint8_t statu; 
//  uint8_t dataInput=0;
//  char *Flash_UUID_ptr =(char *) UUID_ADR;
//  
//  UpdateData_def updateData; 
//  
//  int16_t CRC_res ;
//  
//  uint8_t header_recived=IAP_NO;
//  uint8_t index_recived=IAP_NO;
//  uint8_t len_recived=IAP_NO;
//  uint8_t adr_recived=IAP_NO;
//  uint8_t crc_recived=IAP_NO;
//  uint8_t data_recived=IAP_NO;
//  
//  uint8_t rec_ok=IAP_NO;//单包接收OK
//  uint8_t index=0;
//  
//  uint8_t tempValue;
//  uint32_t time_count=0,timeout=0;
//  uint16_t byte_count=0,data_index=0;
//  
//  //超时机制还没加
//  
//  while(1){
//     data_index=0;
//     header_recived = IAP_NO;
//     index_recived  = IAP_NO;
//     len_recived    = IAP_NO;
//     adr_recived    = IAP_NO;
//     crc_recived    = IAP_NO;
//     data_recived   = IAP_NO;
//     byte_count     = 0;
//     rec_ok=IAP_NO;
//    for(num=0;num<PACK_DATA_LEN;num++) updateData.data[num]=0;//初始化接收数组
//    num=0;
//    while(rec_ok==IAP_NO){
//      while(!(P1IN & GPIO_PIN2));//等待高电平
//      statu = P1IN & GPIO_PIN2;
//      if(statu)//高电平来了。
//      {
//        time_count=0;
//        if(header_recived==IAP_NO){
//           while((P1IN & GPIO_PIN2)==statu){time_count++;}//亮的时间
//            if((time_count>=150)&&(time_count<=300))
//                header_recived=IAP_YES;
//        }
//        else{
//          while((P1IN & GPIO_PIN2)==statu){time_count++;}//亮的时间
//          if(time_count<=30) dataInput +=(1<<num);
//          num++;
//          if(num==8) {
//            num=0;
//            byte_count++;
//            if(index_recived==IAP_NO){
//              if(dataInput==0) {
//                updateData.indexNum = 0;//这个是start
//                index_recived = IAP_YES;
//                /*检测到start信号之后进行的操作：
//                设置看门狗超时
//                
//                */
//              }
//              else{ //if(dataInput == (updateData.indexNum + 1)){
//                updateData.indexNum = dataInput;//index必须按序递增
//                index_recived = IAP_YES;
//              }
////              else
////              {//index错误
////                header_recived = IAP_NO;
////              }
//            }
//            else if(adr_recived==IAP_NO){
//              if(byte_count==2){
//                updateData.addr = dataInput<<8;
//              }
//              else if(byte_count==3){
//                updateData.addr += dataInput;
//                adr_recived=IAP_YES;
//              }else{ 
//                 header_recived = IAP_NO;
//                 index_recived  = IAP_NO;
//                 len_recived    = IAP_NO;
//                 adr_recived    = IAP_NO;
//                 crc_recived    = IAP_NO;
//                 data_recived   = IAP_NO;
//                 byte_count     = 0;
//              }
//              
//            }
//            else if(len_recived==IAP_NO){
//              if(byte_count==4){
//                updateData.lenth = dataInput<<8;
//              }
//              else if(byte_count==5){
//                updateData.lenth += dataInput;
//                len_recived=IAP_YES;
//              }else{ 
//                 header_recived = IAP_NO;
//                 index_recived  = IAP_NO;
//                 len_recived    = IAP_NO;
//                 adr_recived    = IAP_NO;
//                 crc_recived    = IAP_NO;
//                 data_recived   = IAP_NO;
//                 byte_count     = 0;
//              }            
//            }
//            else if(data_recived==IAP_NO){
//                updateData.data[data_index++] = dataInput;
//                if(data_index == updateData.lenth)
//                  data_recived=IAP_YES;
//            }
//            else if(crc_recived==IAP_NO){
//              if(byte_count==(updateData.lenth+6)){
//                updateData.CRC_Value = dataInput<<8;
//              }
//              else if(byte_count==(updateData.lenth+7)){
//                updateData.CRC_Value += dataInput;
//                crc_recived = IAP_YES;
//                rec_ok = IAP_YES;
//                
////                HWREG16(CRC_BASE + OFS_CRCINIRES) = 0xffff;  //set seed
////                HWREG8(CRC_BASE + OFS_CRCDI_L) = updateData.indexNum;
////                HWREG8(CRC_BASE + OFS_CRCDI_L) = (updateData.addr>>8)&0xff;//高位先进
////                HWREG8(CRC_BASE + OFS_CRCDI_L) = updateData.addr & 0xff;
////                HWREG8(CRC_BASE + OFS_CRCDI_L) = (updateData.lenth>>8)&0xff;//高位先进
////                HWREG8(CRC_BASE + OFS_CRCDI_L) = updateData.lenth & 0xff;
////                
////                for(uint16_t index=0;index < updateData.lenth;index++)
////                    HWREG8(CRC_BASE + OFS_CRCDI_L) = updateData.data[index];  //data input
////                CRC_res = HWREG16(CRC_BASE + OFS_CRCINIRES);
////                if(updateData.CRC_Value==CRC_res){
////                    rec_ok = IAP_YES;
////                    //send the ok ACK back to CHG
////                }else{ 
////                     rec_ok = IAP_NO;
////                     header_recived = IAP_NO;
////                     index_recived  = IAP_NO;
////                     len_recived    = IAP_NO;
////                     adr_recived    = IAP_NO;
////                     crc_recived    = IAP_NO;
////                     data_recived   = IAP_NO;
////                     byte_count     = 0;
////                }
//              }
//            }
//          dataInput = 0;
//          }
//        }
//      }
//    }//end of while(rec_ok==IAP_NO)
//    /* ok now we got the data pack,write the data into eeprom*/
//    if(rec_ok==IAP_YES){
//      if(updateData.indexNum==0){//收到start指令
//        //计算CRC
//        sendchar[0]= updateData.indexNum;
//        sendchar[1]= 0xAA;
//        sendchar[2]= 0xBB;
//        sendchar[3]= 0xCC;
//        sendchar[4]= 0xDD;
////        sendchar[1]= *Flash_UUID_ptr++;
////        sendchar[2]= *Flash_UUID_ptr++;
////        sendchar[3]= *Flash_UUID_ptr++;
////        sendchar[4]= *Flash_UUID_ptr++;
//        
//        HWREG16(CRC_BASE + OFS_CRCINIRES) = 0xffff;  //set seed
//        HWREG8(CRC_BASE + OFS_CRCDI_L) = updateData.indexNum;
//        for(uint8_t i=0;i<4;i++)
//          HWREG8(CRC_BASE + OFS_CRCDI_L) = sendchar[i];
//        
//        sendchar[5] = HWREG8(CRC_BASE + OFS_CRCINIRES_H);
//        sendchar[6] = HWREG8(CRC_BASE + OFS_CRCINIRES_L);
//        sendArry(sendchar,7);
//        rec_ok = IAP_NO;
//      }
//      else{
//        //处理
//        //处理完发送回复
//        //计算CRC
//        sendchar[0]= updateData.indexNum;
//        HWREG16(CRC_BASE + OFS_CRCINIRES) = 0xffff;  //set seed
//        HWREG8(CRC_BASE + OFS_CRCDI_L) = updateData.indexNum;
//        sendchar[1] = HWREG8(CRC_BASE + OFS_CRCINIRES_H);
//        sendchar[2] = HWREG8(CRC_BASE + OFS_CRCINIRES_L);
//        sendArry(sendchar,3);
//        rec_ok = IAP_NO;
//      }
//    }
//  }
//}
//
//
//void wrt_flash_arryIAP(uint16_t addr,uint8_t* data,uint8_t len)@"UPDATECODE"
//{
//  unsigned int i;
//  char *Flash_ptr_IAP = (char *)addr;        // Initialize Flash pointer
////  uint16_t gieStatus;
//  //Store current SR register
////  gieStatus = __get_SR_register() & GIE;
//  //Disable global interrupt
//  __disable_interrupt();
//
//  FCTL3 = FWKEY;                            // Clear Lock bit
//  FCTL1 = FWKEY+WRT;                        // Set WRT bit for write operation
//  for(i=0;i<len;i++){
//	  *Flash_ptr_IAP++ = data[i];           // Write a word to flash
//  }
//
//  FCTL1 = FWKEY;                            // Clear WRT bit
//  FCTL3 = FWKEY+LOCK;                       // Set LOCK bit
//  
//  //Reinstate SR register
////   __bis_SR_register(gieStatus);
//  __enable_interrupt();
//}
