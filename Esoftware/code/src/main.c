/*********************************************************
  The entry of the Goccia MCU code.

  Write by Sonic.Xie

  State Key Lab of Solid State Lightting Changzhou China
  All rights reserved.

  Version 5.2

  2013/12/12

*********************************************************/

#include "ActRainbow.h"
#include "core.h"
#include "encode.h"
#include "led.h"

/*============初始化全局变量==================*/
//唤醒事件标志位
EventFlag volatile event_flag = FLAG_Nothing;
//电量 百分比
uint8_t g_powerlever = 1;

uint16_t sec_clock = 0;
float AD_value = 4;

StatuFlag g_SleepDetect = ActiveSleepDetectClose;
void do_lightJob(){
     
    getBatterVolte();
    if((DAY_MODE == CurrentData.Mode)&&
    (POWER_VERF == (*((uint8_t *)POWER_ON_ADDR)))){
      //如果是睡眠监测状态发光，则不新建标识符
      dataReshape();
      //发送彩光之前要把现在的数据保存好。
      WriteCurrentDataToEEPROM();
      init_CurrentData(DAY_MODE);
    }
    SendOutLightData();
    ActiveWorkMode();
}
int main(void)
{
  //初始化
  SYS_INIT();

  _EINT();  //开中断
  for (uint8_t i = 1; i <= 12; i++)
  {
    clock_light_on(i);
    delay_ms(100);
  }
  for (uint8_t i = 1; i <= 12; i++)
  {
    clock_light_off(i);
    delay_ms(100);
  }
  //ledShow();
  //showProgress();
  //showTime();
  //delay_ms(500);
  ActiveWorkMode();
  event_flag = FLAG_WirlessCharge;
  
//  if(POWER_VERF == (*((uint8_t *)POWER_ON_ADDR))){
//    ActiveWorkMode();
//    event_flag = FLAG_WirlessCharge;
//  }
//  else{
//    if(GPIO_getInputPinValue (GPIO_PORT_P1,GPIO_PIN4)
//                                    ==GPIO_INPUT_PIN_LOW){
//        ActiveWorkMode();
//        event_flag = FLAG_WirlessCharge;                     
//    }else{
//        LIS3DH_Init(LIS3DH_ODR_25Hz);
//        delay_ms(500);
//        unActiveMode();
//        event_flag = FLAG_intoStandby;
//    }
//  }
  
  
  while (1)
  {
    //检测事件标志位
    switch(event_flag)
    {
      case FLAG_OutOfStandby:                   //退出待机模式
        getBatterVolte();                       // 判断电压，
        if(AD_value > LOW_VOLTAGE_TO_SBM){
          OutOfStandbyMode();
          event_flag = FLAG_Nothing; 
        }else{
//          RGB_Light_ON(LEDRED);                 //如果电压不够就显示红灯
//          delay_ms(500);
//          RGB_Light_OFF(LEDRED);
          event_flag = FLAG_intoStandby;
        }
         break;

      case FLAG_WaterMarkFull:                  //FIFO满事件
           sec_clock++;
#ifdef DEBUG_LIGHT           
          if((sec_clock%lightOnCycle)==0){
            if(CurrentData.Mode==DAY_MODE){
              clock_light_on(((sec_clock/lightOnCycle)%8)+1);
            }
            else{
              RGB_Light_ON(LEDBLUE);
            }
          }
          GPIO_toggleOutputOnPin(UART_PORT,UART_TX_PIN);
#endif
          if((CurrentData.Mode!=DAY_MODE)&&(CurrentData.Mode!=NIGHT_MODE)){
            OutOfStandbyMode();
          }
          ActRainbow_FIFORoutine();
          
#ifdef DEBUG_LIGHT            
          GPIO_toggleOutputOnPin(UART_PORT,UART_TX_PIN);
          clock_light_color_AllOff();
          RGB_Light_OFF(LEDBLUE);
#endif         
          if(GPIO_getInputPinValue(GPIO_PORT_P1,GPIO_PIN4)
             ==GPIO_INPUT_PIN_LOW){
               event_flag = FLAG_WirlessCharge;
          }else{
               event_flag = FLAG_Nothing;
          }
          break;
          
          
      case FLAG_WirlessCharge:                  //无线充电事件
        if(GPIO_getInputPinValue (GPIO_PORT_P1,GPIO_PIN4)
                                    ==GPIO_INPUT_PIN_LOW){
           getBatterVolte();
           led_charging();
           enterChargingMode(); 
           sec_clock = 0;
        }else{
           event_flag = FLAG_Nothing;
        }
          break;
        
      case FLAG_intoStandby:
          break;
        
      default:
        event_flag = FLAG_Nothing;
        break;
    }
    LPM3;
  }
}









