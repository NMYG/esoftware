/*
 * FLASH_EEPROM.c
 *
 *  Created on: 2013-9-12
 *  Author: SonicZY
 */

#include "ActRainbow.h"
#include "FLASH_EEPROM.h"


char value;                                 // 8-bit value to write to Bank 1


//------------------------------------------------------------------------------
// Erases flash Memory
//------------------------------------------------------------------------------
void erase_Seg(uint16_t segAddr)
{
	char *Flash_ptr = (char *)segAddr;        // Initialize Flash pointer
	FCTL3 = FWKEY;                            // Clear Lock bit
	FCTL1 = FWKEY+ERASE;                      // Set Bank Erase bit
	*Flash_ptr = 0;// Dummy erase byte
	FCTL3 = FWKEY+LOCK;                       // Set LOCK bit
}

void erase_area()
{
	char i; 
     //   uint16_t gieStatus;
        //Store current SR register
     //   gieStatus = __get_SR_register() & GIE;
        //Disable global interrupt
        __disable_interrupt();
        
	for(i=0;i<SEG_NUM;i++){
		erase_Seg(SEG_ADDR+(i*0x201));
	}
        
        //Reinstate SR register
        __enable_interrupt();
        //__bis_SR_register(gieStatus);
}

//------------------------------------------------------------------------------
// Writes a char Value to flash
//------------------------------------------------------------------------------
void wrt_flash_byte(uint16_t addr,uint8_t data)
{

  char *Flash_ptr = (char *)addr;        // Initialize Flash pointer

  FCTL3 = FWKEY;                            // Clear Lock bit
  FCTL1 = FWKEY+WRT;                        // Set WRT bit for write operation

  *Flash_ptr++ = data;                   // Write a word to flash

  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;                       // Set LOCK bit
  
}

void wrt_flash_arry(uint16_t addr,uint8_t* data,uint8_t len)
{
  unsigned int i;
  char *Flash_ptr = (char *)addr;        // Initialize Flash pointer
  __disable_interrupt();

  FCTL3 = FWKEY;                            // Clear Lock bit
  FCTL1 = FWKEY+WRT;                        // Set WRT bit for write operation
  for(i=0;i<len;i++){
	  *Flash_ptr++ = data[i];           // Write a word to flash
  }

  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;                       // Set LOCK bit
  
  //Reinstate SR register
//   __bis_SR_register(gieStatus);
  __enable_interrupt();
}

