
#ifndef __m24c64
#define __m24c64



#define EEPROM_ADDRESS      0x50   //读取ROM数据时的器件地址
#define EEPROM_ID_ADDRESS   0x58   //读取EEPROM ID时的器件地址 （M24C64-D only)

#define PAGE_BYTES 128      //page的大小（byte）
#define PAGE_ADR_OFFSET 7  //page地址偏移，左移

//低5个bit为字节地址，其余的高bit为page地址。
 uint8_t M24C64_ReadByte(uint16_t ReadAddr, uint8_t* Data);
 uint8_t M24C64_WriteByte(uint16_t WriteAddr, uint8_t Data);
 uint8_t M24C64_ReadPage(uint16_t ReadAddr, uint8_t* Data);
 uint8_t M24C64_WritePage(uint16_t WriteAddr, uint8_t* Data);
 uint8_t M24C64_ReadPageWithLen(uint16_t ReadAddr, uint8_t* Data,uint8_t len);
 uint8_t M24C64_WritePageWithLen(uint16_t WriteAddr, uint8_t* Data,uint8_t len);
 uint8_t M24C64_WriteByte_PowerON(uint16_t WriteAddr, uint8_t Data);
 uint8_t M24C64_ReadByte_PowerON(uint16_t ReadAddr, uint8_t* Data);
 uint8_t M24C64_WriteOverPageWithLen(uint16_t WriteAddr,uint8_t* Data,uint8_t len);//跨页写


#endif