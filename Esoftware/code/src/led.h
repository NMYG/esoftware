#ifndef __LED__H
#define __LED__H

#include "ActRainbow.h"
#include "encode.h"

#define CLOCK_GREEN     0x00
#define CLOCK_RED       0x01


extern bool g_pwm_on ;
extern bool g_pwm_show;
extern uint8_t g_rgbLED_pwm;
extern uint8_t headerLED;
void ledShow(void);
void RGB_Light_ON(uint8_t Color);
void RGB_Light_OFF(uint8_t Color);
void RGB_Light_AllOFF(void);
void clock_light_on(uint8_t pos);
void clock_light_off(uint8_t pos);
void clock_light_color_AllOff();
void clock_light_color_AllOn();
void clock_light_toggle(uint8_t pos);//翻转指定的四周的灯

/*灯光效果*/
void led_prolongedMotionless();//长时间未运动
void led_reachGoal();//到达目标
//void led_switchToStepCounter();//切换到计步模式
//void led_switchToSleepMonitor();//切换到睡眠模式
void led_charging();//充电中
void led_chargeFull();//充电满
void showProgress();//显示进度
void ledShow_pwm();
void chargeLED_pwm();
void showTime(); // show RTC time
#endif