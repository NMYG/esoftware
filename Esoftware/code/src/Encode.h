/*
 * encode.h
 *
 *  Created on: 2013-9-10
 *      Author: SonicZY
 */

#ifndef ENCODE_H_
#define ENCODE_H_

#define WHITE       0x07  //000
#define BLUE        0x04  //011
#define GREEN       0x02  //101
#define RED         0x01  //110
#define BLACK       0x00  //111
   
   
#define LEDBLUE        GPIO_PIN2  //011
#define LEDGREEN       GPIO_PIN1  //101
#define LEDRED         GPIO_PIN0  //110
#define LEDWHITE       GPIO_PIN0+GPIO_PIN1+GPIO_PIN2  //000
   
   
#define BAND_NUM       40  //每帧色带数,除了header之外
   
#define BYTE_NUM       10  //每seg除了header之外的字节数   
   
   
extern uint8_t seg;
//extern uint8_t dataBand[BAND_NUM];
//void encode(const uint8_t powerLever,const uint16_t FlashAddress);
void encodeLog(const uint8_t powerLever,const uint16_t FlashAddress);

#endif